STS のインストール
=====

## はじめに

[STS (Spring Tool Suite)](https://spring.io/tools/sts) は Spring Framework の開発をリードする Pivotal 社が提供している IDE (統合開発環境) です。Eclipse を Spring Framework の開発用に最適化したものであり、Eclipse に習熟している開発者はすぐに使いこなすことができます。


## ダウンロード

最新版を利用する場合は https://spring.io/tools/sts にアクセスし、`DOWNLOAD STS` ボタンをクリックしてください。

過去のバージョンを利用する場合は https://spring.io/tools/sts/legacy にアクセスし、任意のバージョンをダウンロードしてください。

本資料では spring-tool-suite-3.8.1.RELEASE-e4.6-win32-x86_64.zip のインストール手順を紹介します。


## インストール

spring-tool-suite-3.8.1.RELEASE-e4.6-win32-x86_64.zip を `C:\` に 展開します。  
![](./images/04_001.JPG)

`C:\sts-bundle\sts-3.8.1.RELEASE\STS.exe` を起動します。

`このファイル開く前に常に警告する` のチェックを外して `実行` をクリックします。  
![](./images/04_002.JPG)

`Use this as the default and do not ask again` にチェックを入れて `OK` を押す。  
![](./images/04_003.JPG)

STS が起動することを確認します。  
![](./images/04_004.JPG)


以上で STS のインストールは完了です。

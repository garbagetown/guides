Lombok のインストール
=====

## はじめに

[Lombok](https://projectlombok.org/) は getter/settere などの冗長な記述 (ボイラープレートコードと呼ばれる) を自動生成するライブラリで、"ロムボク" または "ロンボック" などと発音します。

ソースコードそのものを自動生成するわけではなく、生成されたバイトコードを操作するわけでもありません。コンパイル処理に割り込み、ソースコードにボイラープレートコードが記述されていた状態を作り出すため、ソースコードを簡潔に保ちつつ、自然なバイトコードを生成することができます。


## ダウンロード

最新版を利用する場合は https://projectlombok.org/download.html にアクセスし、`Download!` ボタンをクリックしてください。

過去のバージョンを利用する場合は https://projectlombok.org/all-versions.html にアクセスし、任意のバージョンをダウンロードしてください。

本資料では lombok.jar 1.16.16. のインストール手順を紹介します。


## インストール

lombok.jar をダブルクリックして起動したら、インストールした `STS.exe` が検出されていることを確認して `Install/Update` をクリックします。

`STS.exe` が検出されない場合は `Specify location` ボタンをクリックして、`C:\sts-bundle\sts-3.8.1.RELEASE\STS.exe` を指定してください。  
![](./images/05_001.JPG)

Lombok が正常にインストールされたことを確認したら `OK` をクリックする。  
![](./images/05_002.JPG)


以上で Lombok のインストールは完了です。

Java のインストール
=====

## はじめに

本資料では [JDK(Java SE Development Kit)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) のインストール手順を紹介します。

JRE (Java Runtime Environment) にはコンパイラが付属しておらず、開発には利用できないので注意してください。


## ダウンロード

最新版を利用する場合は http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html にアクセスし、`Accept License Agreement` をチェックして、利用している OS, CPU アーキテクチャ用のインストーラをダウンロードしてください。

過去のバージョンを利用する場合は http://www.oracle.com/technetwork/java/archive-139210.html にアクセスし、任意のバージョンをダウンロードしてください。

本資料では jdk-8u102-windows-x64.exe のインストール手順を紹介します。


## インストール

jdk-8u102-windows-x64.exe を実行し、すべてデフォルトを受け入れてインストールします。  
![](./images/02_001.JPG)  
![](./images/02_002.JPG)  
![](./images/02_003.JPG)  
![](./images/02_004.JPG)  

インストールが完了したら `閉じる` をクリックします。  
![](./images/02_005.JPG)

新規システム環境変数 `JAVA_HOME` の値に `C:\Program Files\Java\jdk1.8.0_102` を設定します。  
![](./images/02_006.JPG)

既存システム環境変数 `PATH` の末尾に `;%JAVA_HOME%\bin` を追加します。  
![](./images/02_007.JPG)

Git Bash を開いて `java -version` コマンドが実行できることを確認します。

```
$ java -version
java version "1.8.0_102"
Java(TM) SE Runtime Environment (build 1.8.0_102-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.102-b14, mixed mode)
```

以上で Java のインストールは完了です。

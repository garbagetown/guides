Maven のインストール
=====

## はじめに

本資料では [Maven](https://maven.apache.org/) のインストール手順を紹介します。

Maven はライブラリの依存性管理ツールで、"メイベン" または "メイバン" などと発音します。同様のツールに [Gradle](https://gradle.org/) がありますが、本資料ではより基本的な Maven を使います。


## ダウンロード

最新版を利用する場合は https://maven.apache.org/download.cgi にアクセスし、apache-maven-x.x.x.-bin.zip をダウンロードしてください。

過去のバージョンを利用する場合は https://maven.apache.org/docs/history.html にアクセスし、任意のバージョンをダウンロードしてください。

本資料では apache-maven-3.3.9-bin.zip のインストール手順を紹介します。


## インストール

apache-maven-3.3.9-bin.zip を `C:\` に展開します。

新規システム環境変数 `M3_HOME` の値に `C:\apache-maven-3.3.9` を設定します。  
![](./images/03_001.JPG)

既存システム環境変数 `PATH` の末尾に `;%M3_HOME%\bin` を追加します。  
![](./images/03_002.JPG)

Git Bash を開いて `mvn -version` コマンドが実行できることを確認します。

```sh
$ mvn -version
Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-11T01:41:47+09:00)
Maven home: C:\apache-maven-3.3.9
Java version: 1.8.0_121, vendor: Oracle Corporation
Java home: C:\Program Files\Java\jdk1.8.0_121\jre
Default locale: ja_JP, platform encoding: MS932
OS name: "windows 7", version: "6.1", arch: "amd64", family: "dos"
```

以上で Maven のインストールは完了です。

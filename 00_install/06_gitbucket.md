GitBucket
=====

以下、CentOS 7 上に GitBucket をインストールする手順を示します。

## ライブラリのインストール

GitBash から CentOS 7 に 一般ユーザとして SSH でログインします。`xxx.xxx.xxx.xxx` は自身の環境に読み替えてください。
```sh
$ ssh xxx.xxx.xxx.xxx
(snip)
```

`yum update` して依存ライブラリの情報を最新化します。下記例ではすでに最新化されていますが、最新化されていない場合は数分から十数分掛かる場合があるので注意してください。
```sh
$ sudo yum -y update
[sudo] password for ddmadmin:
読み込んだプラグイン:fastestmirror, langpacks
(snip)
Determining fastest mirrors
 * epel: ftp.jaist.ac.jp
No packages marked for update
```

以下のライブラリをインストールします。
- httpd
- wget
- git
- java
- tomcat

```sh
$ sudo yum -y install httpd wget git java-1.8.0-openjdk-devel tomcat
[sudo] password for root:
読み込んだプラグイン:fastestmirror, langpacks
(snip)
依存性関連をインストールしました:
  apache-commons-collections.noarch 0:3.2.1-22.el7_2                                 apache-commons-daemon.x86_64 0:1.0.13-6.el7
  apache-commons-dbcp.noarch 0:1.4-17.el7                                            apache-commons-logging.noarch 0:1.1.2-7.el7
  apache-commons-pool.noarch 0:1.6-9.el7                                             apr.x86_64 0:1.4.8-3.el7
  apr-util.x86_64 0:1.5.2-6.el7                                                      avalon-framework.noarch 0:4.3-10.el7
  avalon-logkit.noarch 0:2.1-14.el7                                                  ecj.x86_64 1:4.2.1-8.el7
  geronimo-jms.noarch 0:1.1.1-19.el7                                                 geronimo-jta.noarch 0:1.1.1-17.el7
  httpd-tools.x86_64 0:2.4.6-40.el7.centos.4                                         java-1.8.0-openjdk.x86_64 1:1.8.0.102-1.b14.el7_2
  java-1.8.0-openjdk-headless.x86_64 1:1.8.0.102-1.b14.el7_2                         javamail.noarch 0:1.4.6-8.el7
  libgnome-keyring.x86_64 0:3.8.0-3.el7                                              log4j.noarch 0:1.2.17-15.el7
  mailcap.noarch 0:2.1.41-2.el7                                                      perl-Error.noarch 1:0.17020-2.el7
  perl-Git.noarch 0:1.8.3.1-6.el7_2.1                                                perl-TermReadKey.x86_64 0:2.30-20.el7
  tomcat-el-2.2-api.noarch 0:7.0.54-2.el7_1                                          tomcat-jsp-2.2-api.noarch 0:7.0.54-2.el7_1
  tomcat-lib.noarch 0:7.0.54-2.el7_1                                                 tomcat-servlet-3.0-api.noarch 0:7.0.54-2.el7_1
  xalan-j2.noarch 0:2.7.1-23.el7                                                     xerces-j2.noarch 0:2.11.0-17.el7_0
  xml-commons-apis.noarch 0:1.4.01-16.el7                                            xml-commons-resolver.noarch 0:1.2-15.el7

完了しました!
```

httpd を起動します。併せてシステム起動時に自動で起動するよう設定します。
```sh
$ sudo systemctl start httpd
$ sudo systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
```

tomcat を起動します。併せてシステム起動時に自動で起動するよう設定します。
```sh
$ sudo systemctl start tomcat
$ sudo systemctl enable tomcat
Created symlink from /etc/systemd/system/multi-user.target.wants/tomcat.service to /usr/lib/systemd/system/tomcat.service.
```

以上でライブラリのインストールと設定は完了です。


## GitBucket のインストール

GitBucket をインストールします。
```sh
$ sudo wget https://github.com/gitbucket/gitbucket/releases/download/4.5/gitbucket.war -P /var/lib/tomcat/webapps/
--2016-10-04 09:51:32--  https://github.com/gitbucket/gitbucket/releases/download/4.5/gitbucket.war
(snip)
2016-10-04 09:51:39 (8.46 MB/s) - `/var/lib/tomcat/webapps/gitbucket.war' へ保存完了 [47136745/47136745]
```

GitBucket 絵文字プラグインをインストールします。予め `/usr/share/tomcat/.gitbucket/` 以下に `plugins` ディレクトリを作成する必要がある点に注意してください。
```sh
$ sudo mkdir /usr/share/tomcat/.gitbucket/plugins
$ sudo wget https://github.com/gitbucket/gitbucket-emoji-plugin/releases/download/4.3.0/gitbucket-emoji-plugin_2.11-4.3.0.jar -P /usr/share/tomcat/.gitbucket/plugins
--2016-10-04 10:18:00--  https://github.com/gitbucket/gitbucket-emoji-plugin/releases/download/4.3.0/gitbucket-emoji-plugin_2.11-4.3.0.jar
(snip)
2016-10-04 10:18:04 (1.96 MB/s) - `/usr/share/tomcat/.gitbucket/gitbucket-emoji-plugin_2.11-4.3.0.jar' へ保存完了 [4123557/4123557]
```

同様に GitBucket Gist プラグインをインストールします。
```sh
$ sudo wget https://github.com/gitbucket/gitbucket-gist-plugin/releases/download/4.3.0/gitbucket-gist-plugin_2.11-4.3.0.jar -P /usr/share/tomcat/.gitbucket/plugins
--2016-10-04 10:18:56--  https://github.com/gitbucket/gitbucket-gist-plugin/releases/download/4.3.0/gitbucket-gist-plugin_2.11-4.3.0.jar
(snip)
2016-10-04 10:18:59 (370 KB/s) - `/usr/share/tomcat/.gitbucket/gitbucket-gist-plugin_2.11-4.3.0.jar' へ保存完了 [379840/379840]
```

同様に GitBucket Network プラグインをインストールします。
```sh
$ sudo wget https://github.com/mrkm4ntr/gitbucket-network-plugin/releases/download/1.1/gitbucket-network-plugin_2.11-1.1.jar -P /usr/share/tomcat/.gitbucket/plugins/
--2016-10-04 10:27:32--  https://github.com/mrkm4ntr/gitbucket-network-plugin/releases/download/1.1/gitbucket-network-plugin_2.11-1.1.jar
(snip)
2016-10-04 10:27:34 (221 KB/s) - `/usr/share/tomcat/.gitbucket/plugins/gitbucket-network-plugin_2.11-1.1.jar' へ保存完了 [152781/152781]
```

同様に GitBuckete h2 バックアッププラグインをインストールします。
```sh
$ sudo wget https://github.com/gitbucket-plugins/gitbucket-h2-backup-plugin/releases/download/1.3.0/gitbucket-h2-backup.jar -P /usr/share/tomcat/.gitbucket/plugins/
--2016-10-04 10:24:53--  https://github.com/gitbucket-plugins/gitbucket-h2-backup-plugin/releases/download/1.3.0/gitbucket-h2-backup.jar
(snip)
2016-10-04 10:24:56 (147 KB/s) - `/usr/share/tomcat/.gitbucket/plugins/gitbucket-h2-backup.jar' へ保存完了 [24975/24975]
```

以上で GitBucket のインストールは完了です。

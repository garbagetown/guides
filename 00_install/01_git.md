Git for Windows のインストール
=====

## はじめに

本資料では [Git for Windows](https://git-for-windows.github.io/) のインストール手順を紹介します。

Git for Windows は Windows で利用できる Git クライアントのひとつです。Git そのものについては下記リンクを参照してください。

- [サルでもわかるGit入門 〜バージョン管理を使いこなそう〜 | どこでもプロジェクト管理バックログ](http://www.backlog.jp/git-guide/ "サルでもわかるGit入門 〜バージョン管理を使いこなそう〜 | どこでもプロジェクト管理バックログ")
- [こわくない Git](https://www.slideshare.net/kotas/git-15276118 "こわくない Git")

Git for Windows はキーボードで操作する CUI クライアントです。マウスで操作する GUI クライアントも存在しますが、以下に挙げる課題があるため、本資料では CUI を利用します。

- 費用
	- 有償、非商用に限り無償、ライセンス登録すれば無償など
- 学習コスト
	- ツールごとに使い方を覚えなければならない
	- Git の機能を直感的に扱えないものもある


## ダウンロード

最新版を利用する場合は https://git-for-windows.github.io/ にアクセスし、`Download` ボタンをクリックしてください。

過去のバージョンを利用する場合は https://github.com/git-for-windows/git/releases/ にアクセスし、任意のバージョンをダウンロードしてください。

本資料では Git-2.10.1-64-bit.exe のインストール手順を紹介します。


## インストール

Git-2.10.1-64-bit.exe を実行してください。セキュリティの警告が表示される場合は `実行` をクリックします。  
![](./images/01_001.JPG)  

インストーラが起動するので `Next` をクリックします。  
![](./images/01_002.JPG)  

デフォルトのまま `Next` をクリックします。  
![](./images/01_003.JPG)  

デフォルトのまま `Next` をクリックします。  
![](./images/01_004.JPG)  

デフォルトのまま `Next` をクリックします。  
![](./images/01_005.JPG)  

デフォルトのまま `Next` をクリックします。  
![](./images/01_006.JPG)  

デフォルトのまま `Next` をクリックします。  
![](./images/01_007.JPG)  

デフォルトのまま `Next` をクリックします。  
![](./images/01_008.JPG)  

デフォルトのまま `Install` をクリックします。  
![](./images/01_009.JPG)  

インストールが正常に完了したら `Launch Git Bash` をチェックして `Finish` をクリックします。  
![](./images/01_010.JPG)  

Git Bash が起動することを確認します。  
![](./images/01_011.JPG)  


以上で Git for Windows のインストールは完了です。

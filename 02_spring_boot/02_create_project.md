プロジェクトの作成
=====

## 概要

本資料では、以下の画面を持つプロジェクトを作成します。

- トップ画面  
![](images/02_001.JPG)  
  
  
- 一覧画面  
![](images/02_002.JPG)  
  
  
- 登録画面  
![](images/02_003.JPG)  
  
  
- 更新画面  
![](images/02_004.JPG)  
  
  
- 削除画面  
![](images/02_005.JPG)  
  

各画面と URL のマッピングは以下のとおりです。

| 名称 | パス | HTTPメソッド | パラメータ | ビュー名 |
|:-----|:-----|:-------------|:-----------|:---------|
| トップ画面表示 | /             | GET  |    | /index |
| 一覧画面表示   | /users        | GET  |    | users/index |
| 登録画面表示   | /users/create | GET  |    | users/create |
| 登録処理       | /users/create | POST |    | /users にリダイレクト |
| 更新画面表示   | /users/update | GET  | id | users/update |
| 更新処理       | /users/update | POST |    | /users にリダイレクト |
| 削除画面表示   | /users/delete | GET  | id | user/delete |
| 削除処理       | /users/delete | POST |    | /users にリダイレクト |
| 検索処理       | /users/search | GET  | name, email | users/index |


## プロジェクトの作成

Spring Boot プロジェクトは [Spring Initializr](https://start.spring.io/) で作成すると便利です。Spring Initializr には以下のインタフェースがあります。

- ブラウザ (https://start.spring.io)
- [Spring Boot CLI](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#cli-init) または cURL などのコマンドライン
- STS, IntelliJ IDEA Ultimate または NetBeans などの IDE

本資料では STS を使います。  
  

## STS によるプロジェクトの作成

STS を起動したら新規プロジェクトを作成します。パッケージエクスプローラで右クリックし、コンテキストメニューから `New` -> `Spring Starter Project` を選択します。  
![](images/02_006.JPG)  
  

プロジェクト名その他を入力して `Next` をクリックします。  

|項番|項目|値|備考|
|-|-|-|-|
|1|Name|sample|-|
|2|Location|C:\work\springboot\sample|作成場所をデフォルトから変更|
|3|Group|sample|-|
|4|Artifact|sample|-|
|5|Package|sample|-|

![](images/02_007.JPG)  
  

使用する依存性を指定して `Next` をクリックします (資料の構成上、`Security` は後で追加します)。それぞれの依存性が提供する機能は以下のとおり。

|項番|依存性|機能|
|-|-|-|
|1|DevTools|開発支援ツール。リソースの変更を検知して Spring コンテナを再作成するなどの機能を提供する。開発時のみ利用する|
|2|Lombok|開発支援ツール。コード生成機能などを提供する|
|3|JPA|データベースアクセス機能を提供する|
|4|H2|インメモリデータベース。開発時のみ利用する|
|5|Thymeleaf|ビューテンプレートを提供する|
|6|Web|Web アプリケーション機能を提供する|

![](images/02_008.JPG)  
  
  
デフォルトのまま `Finish` をクリックします。  
![](images/02_009.JPG)  
  

作成したプロジェクトがパッケージエクスプローラに表示されることを確認します。  
![](images/02_010.JPG)  


## git によるバージョン管理

作成したプロジェクトを git でバージョン管理しましょう。Git Bash を起動してプロジェクトを作成したディレクトリに移動します。
```sh
$ cd /c/work/springboot/sample/
```

あらかじめ Spring Initializr が .gitignore を作成していますが、`bin` ディレクトリが足りていないので追加します。
```sh
$ echo "\n\nbin/" >> .gitignore 

$ cat .gitignore 
target/
!.mvn/wrapper/maven-wrapper.jar

### STS ###
.apt_generated
.classpath
.factorypath
.project
.settings
.springBeans

### IntelliJ IDEA ###
.idea
*.iws
*.iml
*.ipr

### NetBeans ###
nbproject/private/
build/
nbbuild/
dist/
nbdist/
.nb-gradle/

bin/
```

git リポジトリとして初期化します。
```sh
$ git init
Initialized empty Git repository in C:/work/springboot/sample/.git/
```

カレントディレクトリの内容をリポジトリに追加します。
```sh
$ git add .
```

追加した内容に間違いがないことを確認します。
```sh
$ git status
# On branch master
#
# Initial commit
#
# Changes to be committed:
#   (use "git rm --cached <file>..." to unstage)
#
#	new file:   .gitignore
#	new file:   .mvn/wrapper/maven-wrapper.jar
#	new file:   .mvn/wrapper/maven-wrapper.properties
#	new file:   mvnw
#	new file:   mvnw.cmd
#	new file:   pom.xml
#	new file:   src/main/java/sample/SampleApplication.java
#	new file:   src/main/resources/application.properties
#	new file:   src/test/java/sample/SampleApplicationTests.java
#
```

最後にリポジトリに追加した内容をコミットします。
```
$ git commit -m "プロジェクトを作成"
[master (root-commit) 2c1462c] プロジェクトを作成
 8 files changed, 506 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 .mvn/wrapper/maven-wrapper.jar
 create mode 100644 .mvn/wrapper/maven-wrapper.properties
 create mode 100755 mvnw
 create mode 100644 mvnw.cmd
 create mode 100644 pom.xml
 create mode 100644 src/main/java/sample/SampleApplication.java
 create mode 100644 src/main/resources/application.properties
 create mode 100644 src/test/java/sample/SampleApplicationTests.java
```

---
以上でプロジェクトの作成は完了です。次はトップ画面を作成しましょう。

- [トップ画面](./03_hello_spring_boot.md)

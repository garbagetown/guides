単体テスト
=====

以下、各モジュールのユニットテストについて紹介する。

## バリデータのテスト

バリデータは他のコンポーネントに依存しないため、通常の JUnit テストケースとして作成する。軽量なテストが可能であり、またバリデータには業務ロジックの一部が含まれるため、積極的に単体テストを作成すること。

```java
package sample.app.users;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

public class UserFormValidatorTest {

    UserFormValidator userFormValidator;

    @Before
    public void setup() {
        userFormValidator = new UserFormValidator(); // (1)
    }

    @Test
    public void testValidate_001() {

        UserForm form = testForm(); // (2)

        Errors errors = new BeanPropertyBindingResult(form, "");
        userFormValidator.validate(form, errors); // (3)

        assertFalse(errors.hasErrors()); // (3)
    }

    @Test
    public void testValidate_002() {

        UserForm form = testForm();
        form.setPassword2("password2"); // (4)

        Errors errors = new BeanPropertyBindingResult(form, "");
        userFormValidator.validate(form, errors); // (4)

        assertFalse(errors.hasFieldErrors("name"));
        assertFalse(errors.hasFieldErrors("email"));
        assertFalse(errors.hasFieldErrors("password"));
        assertTrue(errors.hasFieldErrors("password2")); // (4)
    }

    private UserForm testForm() { // (2)
        UserForm form = new UserForm();
        form.setName("name");
        form.setEmail("email");
        form.setPassword("password");
        form.setPassword2("password");
        return form;
    }
}
```

各記述内容の詳細は以下の通り。

|項番|内容|詳細|
|-|-|-|
|1| `userFormValidator = new UserFormValidator()` | テスト対象クラスのインスタンスを生成する |
|2| `UserForm form = testForm()` | ヘルパメソッドを使って正常な入力内容のフォームオブジェクトを生成する |
|3| `assertFalse(errors.hasErrors())` | 入力チェックエラーが発生していないことを確認する |
|4| `form.setPassword2("password2")` | 不正な状態に書き換えたフォームオブジェクトでは入力チェックエラーが発生することを確認する |


## サービスのテスト

サービスは多くの場合リポジトリに依存するため、`Mockito` でリポジトリのモックを作成してテストする。軽量なテストが可能であり、またサービスには業務ロジックが含まれるため、積極的に単体テストを作成すること。

```java
package sample.domain.service.users;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import sample.domain.model.User;
import sample.domain.repository.UsersRepository;

@RunWith(MockitoJUnitRunner.class) // (1)
public class UsersServiceTest {

    @Mock // (2)
    UsersRepository usersRepository;

    @InjectMocks // (3)
    UsersService usersService;

    @Test
    public void testFindAll_001() {

        List<User> value = IntStream.rangeClosed(1, 3).boxed()
                .map(i -> toUser(i))
                .collect(Collectors.toList());
        when(usersRepository.findAll()).thenReturn(value); // (4)

        List<User> list = usersService.findAll();
        assertThat(list.size(), is(3)); // (5)

        User user = list.get(0);
        assertThat(1, is(user.getId()));
        assertThat("user1", is(user.getName()));
        assertThat("user1@example.com", is(user.getEmail()));
        assertThat("xxx", is(user.getPassword()));
    }

    private User toUser(int i) {
        User user = new User();
        user.setId(i);
        user.setName("user" + i);
        user.setEmail("user" + i + "@example.com");
        user.setPassword("xxx");
        return user;
    }
}
```

各記述内容の詳細は以下の通り。

|項番|内容|詳細|
|-|-|-|
|1| `@RunWith(MockitoJUnitRunner.class)` | モックオブジェクトを作成し、テスト対象のオブジェクトに設定する Runner を指定する |
|2| `@Mock` | 依存するリポジトリのモックを作成することを示す |
|3| `@InjectMocks` | テスト対象オブジェクトにモックを設定することを示す |
|4| `when(usersRepository.findAll()).thenReturn(value);` | モックオブジェクトの `findAll` が呼び出された場合は `value` を返却するよう設定する |
|5| `assertThat(list.size(), is(3));` | サービスクラスの処理結果が正しいことを確認する |


## リポジトリのテスト

リポジトリは `@DataJpaTest` を使ってインメモリデータベース上でテストすることができる。比較的軽量なテストが可能であり、また追加したクエリには業務要件を含むことが多いため、積極的に単体テストを作成すること。

```java
package sample.domain.repository;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import sample.domain.model.User;

@RunWith(SpringRunner.class) // (1)
@DataJpaTest // (2)
public class UsersRepositoryTest {

	@Autowired
	private TestEntityManager entityManager; // (3)

	@Autowired
	private UsersRepository usersRepository;

	@Before
	public void setUp() {
		IntStream.rangeClosed(4, 9).boxed()
		.map(i -> toUser(i))
		.collect(Collectors.toList())
		.forEach(u -> entityManager.persist(u)); // (3)
	}

	@Test
	public void testFindOneByEmail_001() {
		User user1 = usersRepository.findOneByEmail("user1@example.com");
		assertThat("ユーザ1", is(user1.getName())); // (4)

		User user4 = usersRepository.findOneByEmail("user4@example.com");
		assertThat("user4", is(user4.getName())); // (4)
	}

	private User toUser(int i) {
		User user = new User();
		user.setId(null); // (5)
		user.setName("user" + i);
		user.setEmail("user" + i + "@example.com");
		user.setPassword("xxx");
		return user;
	}
}
```

各記述内容の詳細は以下の通り。

|項番|内容|詳細|
|-|-|-|
|1| `@RunWith(SpringRunner.class)` | Spring の単体テストサポートを利用することを示す |
|2| `DataJpaTest` | リポジトリのテストであることを指定する |
|3| `TestEntityManager entityManager` | テスト用のエンティティマネージャを使ってテストデータを作成する |
|4| `assertThat("ユーザ1", is(user1.getName()))` | data.sql に記述したデータが取得できること、および `TestEntityManager` で登録したデータが取得できることを確認する |
|5| `user.setId(null);` | id は JPA が設定するため null を指定する |


## コントローラのテスト

コントローラは Spring コンテナに依存するため、`SpringRunner` を使ってテストする。テストケースごとに Spring コンテナを作成、起動するため、重量なテストとなる。費用対効果を考慮して単体テストを作成すること。

```java
package sample.app.users;

import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import sample.domain.model.User;
import sample.domain.service.users.UsersService;

@RunWith(SpringRunner.class) // (1)
@WebMvcTest(UsersController.class) // (2)
public class UsersControllerTest {

    @Autowired
    MockMvc mockMvc; // (3)

    @MockBean // (4)
    UsersService usersService;

    @MockBean
    UserFormValidator validator;

    @Before
    public void setup() {
        when(validator.supports(anyObject())).thenReturn(true);
    }

    @Test
    public void testIndex_001() throws Exception {

        List<User> value = IntStream.rangeClosed(1, 3).boxed()
                .map(i -> toUser(i))
                .collect(Collectors.toList());
        when(usersService.findAll()).thenReturn(value);

        mockMvc.perform(get("/users")) // (5)
            .andExpect(status().isOk())
            .andExpect(view().name("users/index"))
            .andExpect(model().attributeExists("users"))
            .andExpect(content().string(containsString("user1@example.com")))
            .andExpect(content().string(containsString("user2@example.com")))
            .andExpect(content().string(containsString("user3@example.com")));
    }

    @Test
    public void testCreate() throws Exception {
        mockMvc.perform(get("/users/create"))
            .andExpect(status().isOk())
            .andExpect(view().name("users/create"));
    }

    @Test
    public void testSave() throws Exception {
        mockMvc.perform(post("/users/save")
                .param("name", "user1")
                .param("email", "user1@example.com")
                .param("password", "password")
                .param("password2", "password"))
            .andExpect(status().is(302))
            .andExpect(view().name("redirect:/users/"))
            .andExpect(flash().attributeExists("success"));
    }

    private User toUser(int i) {
        User user = new User();
        user.setId(i);
        user.setName("user" + i);
        user.setEmail("user" + i + "@example.com");
        user.setPassword("xxx");
        return user;
    }
}
```

各記述内容の詳細は以下の通り。

|項番|内容|詳細|
|-|-|-|
|1| `@RunWith(SpringRunner.class)` | Spring の単体テストサポートを利用することを示す |
|2| `@WebMvcTest(UsersController.class)` | 指定したコントローラのみをコンテナに登録することを示す |
|3| `MockMvc mockMvc;` | HTTP サーバのモックオブジェクトを使用する |
|4| `@MockBean` | `WebMvcTest` で指定したクラスに設定するモックを指定する |
|5| `mockMvc.perform(get("/users"))` | HTTP サーバのモックオブジェクトを使用してコントローラの振る舞いをテストする |

ユーザ編集画面の作成
=====

この章では以下の内容を紹介します。

- バリデーションのグループ化
- 更新処理

---
## ブランチの作成

これまで同様、作業用ブランチを作成して移動します。

```sh
$ git branch
* master

$ git checkout -b 06_update_user
Switched to a new branch '06_update_user'

$ git branch
* 06_update_user
  master
```


---
## ビューテンプレートの変更

まず、一覧画面から更新画面へ遷移するアンカーを追加します。`src/main/resources/templates/users/index.html` を以下のように変更してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
(snip)
	<table>
		<thead>
			<tr>
				<th>id</th>
				<th>名前</th>
				<th>メールアドレス</th>
				<th></th> <!-- (6-1) -->
			</tr>
		</thead>
		<tbody>
			<tr th:each="user : ${users}">
				<td th:text="${user.id}"></td>
				<td th:text="${user.name}"></td>
				<td th:text="${user.email}"></td>
				<td><a th:href="@{/users/update(id=${user.id})}">更新</a></td> <!-- (6-2) -->
			</tr>
		</tbody>
	</table>
(snip)
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|6-1| `<th></th>` | 空のヘッダ列を追加する |
|6-2| `<td><a th:href="@{/users/update(id=${user.id})}">更新</a></td>` | リクエストパラメータ `id` に各ユーザの id を指定したパスを生成する |


## リポジトリの変更

登録処理の場合と同様にメールアドレスの存在チェックをすると、メールアドレスを変更しなかった場合に自分自身のメールアドレスと重複してしまいます。

自分自身以外に重複するメールアドレスが存在していないことを確認するために、リポジトリインタフェースにメールアドレスと id をキーにデータを一件取得するメソッドを追加しましょう。

```java
package sample.domain.repository;
(snip)
import sample.domain.model.User;

public interface UsersRepository extends JpaRepository<User, Integer> {
	(snip)
	User findOneByEmailAndIdNot(String email, Integer id); // (6-1)
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|6-1| `findOneByEmailAndIdNot(String email, Integer id)` | `email` の値が一致して `id` の値が一致しない `User` を一件取得するメソッドを自動生成する |


## サービスクラスの変更

次に、サービスクラス `src/main/java/sample/domain/service/users/UsersService.java` に更新対象ユーザの検索処理と、ユーザ更新処理を追加します。パスワードは入力された場合のみ更新することとします。

```java
package sample.domain.service.users;

import org.springframework.util.StringUtils;
(snip)
public class UsersService {
	(snip)
	@Transactional(readOnly = true)
	public User findById(Integer id) {
		return usersRepository.findOne(id); // (6-1)
	}

	public User update(User user) {
		if (usersRepository.findOneByEmailAndIdNot(user.getEmail(), user.getId()) != null) {
			throw new EmailAlreadyExistException();
		}
		
		if (StringUtils.isEmpty(user.getPassword())) { // (6-2)
			String password = usersRepository.findOne(user.getId()).getPassword();
			user.setPassword(password);
		}
		return usersRepository.save(user);
	}
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|6-1| `return usersRepository.findOne(id)` | JpaRepository が提供する `findOne` を使って指定された `id` のユーザを取得する | 
|6-2| `StringUtils.isEmpty(user.getPassword())` | パスワードが入力されていない場合はデータベースに保存されているパスワードを再設定する |


## ビューテンプレートの作成

続いて、ビューテンプレートを作成します。以下の内容で `src/main/resources/templates/users/update.html` を作成してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
<head>
<meta charset="UTF-8" />
<title>更新</title>
</head>
<body>
	<h1>ユーザ更新</h1>
	<form th:action="@{/users/update}" method="post" th:object="${userForm}"> <!-- (6-1) -->
		<div th:if="${#fields.hasAnyErrors()}">Error!</div>
		<div>
			<label for="name">名前:</label>
			<input type="text" th:field="*{name}" />
		</div>
		<div>
			<label for="email">メールアドレス:</label>
			<input type="text" th:field="*{email}" />
		</div>
		<div>
			<label for="password">パスワード:</label>
			<input type="password" th:field="*{password}" />
		</div>
		<div>
			<label for="password2">パスワード (再入力):</label>
			<input type="password" th:field="*{password2}" />
		</div>
		<div>
			<input type="hidden" th:field="*{id}" /> <!-- (6-2) -->
			<button>更新</button>
		</div>
	</form>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。なお、登録処理と重複する説明は省略します。

|項番|内容|詳細|
|-|-|-|
|6-1| `th:object="${userForm}"` | 登録処理で使った `UserForm` を更新処理でも使う |
|6-2| `<input type="hidden" th:field="*{id}" />` | 更新対象ユーザの `id` を `hidden` フィールドに保持する |


## フォームの変更

ここで、ビューテンプレートで指定したとおり登録処理で使った `UserForm` を更新処理でも使えるように変更します。`src/main/java/sample/app/users/UserForm.java` を以下のように変更してください。

```java
package sample.app.users;

import javax.validation.groups.Default;
(snip)
public class UserForm {

	public static interface Create extends Default {}; // (6-1)
	
	private Integer id; // (6-2)
(snip)
	@NotNull(groups = Create.class) // (6-3)
	@Size(min = 8, max = 60)
	private String password;

	@NotNull(groups = Create.class)
	@Size(min = 8, max = 60)
	private String password2;
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|6-1| `public static interface Create extends Default {}` | バリデーションをグループ化するためのマーカーインタフェース。このグループを指定してバリデーションを実行する際、`groups` を指定していないアノテーションも検査対象となるよう `Default` グループを継承する |
|6-2| `private Integer id` | hidden フィールドに追加した `id` の値を受け取るフィールド |
|6-3| `groups = Create.class` | `Create` グループが指定された場合のみ当該バリデーションを実行する |


## コントローラの変更

最後に、作成したフォームクラスとビューテンプレート、および変更したサービスクラスを使って更新処理を行うよう `UserController` を変更します。

```java
package sample.app.users;

import sample.app.users.UserForm.Create;
import org.springframework.web.bind.annotation.RequestParam;
(snip)
public class UsersController {
(snip)
	@PostMapping("create")
	public String create(@Validated(Create.class) UserForm form, // (6-1)
			BindingResult result,
			RedirectAttributes redirect) {
(snip)
	}

	@GetMapping("update") // (6-2)
	public String update(@RequestParam("id") Integer id, // (6-3)
		UserForm form) { // (6-4)
		User user = usersService.findById(id);
		BeanUtils.copyProperties(user, form); // (6-5)
		return "users/update";
	}
	
	@PostMapping("update") // (6-6)
	public String update(@Validated UserForm form, // (6-7)
			BindingResult result,
			RedirectAttributes redirect) {
		
		if (result.hasErrors()) {
			return "users/update";
		}
		
		User user = toUser(form);
		try {
			usersService.update(user);
		} catch (EmailAlreadyExistException e) {
			result.addError(new ObjectError("email", e.getMessage()));
			return "users/update";
		}
		
		redirect.addFlashAttribute("success", "更新成功");
		
		return "redirect:/users/";
	}
(snip)
}
```

記述内容の詳細は以下のとおりです。こちらも登録処理と重複する説明は省略します。

|項番|内容|詳細|
|-|-|-|
|6-1| `@Validated(Create.class) UserForm form` | 登録処理ではパスワード入力必須をチェックする `Create` グループのバリデーションを行う |
|6-2| `@GetMapping("update")` | `update` に対する GET リクエストを処理することを示す |
|6-3| `@RequestParam("id") Integer id` | リクエストパラメータ `id` の値を Integer 型の変数として受け取る |
|6-4| `UserForm form` | `@ModelAttribute` で設定した `UserForm` を受け取る |
|6-5| `BeanUtils.copyProperties(user, form)` | リクエストパラメータ `id` で指定されたユーザの情報を `UserForm` にコピーする |
|6-6| `@PostMapping("update")` | `update` に対する POST リクエストを処理することを示す |
|6-7| `@Validated UserForm form` | 更新処理では `Default` グループのバリデーションを行う |


## 登録処理の確認

まず、変更した登録処理の動作を再確認します。

ブラウザから再度 http://localhost:8080/users/crete にアクセスし、パスワード欄とパスワード(再入力)欄が入力されていない場合は入力エラーになることを確認します。  
![](images/06_001.JPG)  
![](images/06_002.JPG)  
  

両者が適切に入力されている場合は登録処理に成功することも確認しましょう。  
![](images/06_003.JPG)  
![](images/06_004.JPG)  
  

## 更新処理の確認

続いて、作成した更新処理の動作を確認します。

ブラウザから再度 http://localhost:8080/users/ にアクセスし、一覧画面に更新画面へのアンカーが表示されることを確認します。  
![](images/06_005.JPG)  
  

アンカーをクリックして更新画面に遷移し、当該ユーザの情報が表示されることを確認します。セキュリティ制限によりパスワード入力欄の初期値は表示されません。  
![](images/06_006.JPG)  
  

パスワードを入力せずに更新が行えることを確認してください。  
![](images/06_007.JPG)  
![](images/06_008.JPG)  
  

パスワードを入力して更新が行えることも確認しましょう。  
![](images/06_009.JPG)  
![](images/06_010.JPG)  
  

併せて Hibernate が発行した SQL のログがコンソールに出力されていることも確認しておきましょう。
```
2017-04-02 07:37:00.739 DEBUG 44611 --- [nio-8080-exec-7] org.hibernate.SQL                        : 
    select
        user0_.id as id1_0_0_,
        user0_.email as email2_0_0_,
        user0_.name as name3_0_0_,
        user0_.password as password4_0_0_ 
    from
        users user0_ 
    where
        user0_.id=?
2017-04-02 07:37:00.740 TRACE 44611 --- [nio-8080-exec-7] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [INTEGER] - [4]
2017-04-02 07:37:00.741 DEBUG 44611 --- [nio-8080-exec-7] org.hibernate.SQL                        : 
    update
        users 
    set
        email=?,
        name=?,
        password=? 
    where
        id=?
2017-04-02 07:37:00.741 TRACE 44611 --- [nio-8080-exec-7] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [VARCHAR] - [user4@example.com]
2017-04-02 07:37:00.742 TRACE 44611 --- [nio-8080-exec-7] o.h.type.descriptor.sql.BasicBinder      : binding parameter [2] as [VARCHAR] - [ユーザ4a]
2017-04-02 07:37:00.742 TRACE 44611 --- [nio-8080-exec-7] o.h.type.descriptor.sql.BasicBinder      : binding parameter [3] as [VARCHAR] - [password]
2017-04-02 07:37:00.742 TRACE 44611 --- [nio-8080-exec-7] o.h.type.descriptor.sql.BasicBinder      : binding parameter [4] as [INTEGER] - [4]
```

---
## コミットとマージ

以上でユーザ編集画面の作成は完了です。ここまでの作業内容を git に保存しましょう。

`git status` コマンドでブランチの状態を確認します。
```
$ git status
# On branch 06_update_user
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   src/main/java/sample/app/users/UserForm.java
#	modified:   src/main/java/sample/app/users/UsersController.java
#	modified:   src/main/java/sample/domain/repository/UsersRepository.java
#	modified:   src/main/java/sample/domain/service/users/UsersService.java
#	modified:   src/main/resources/templates/users/index.html
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/main/resources/templates/users/update.html
no changes added to commit (use "git add" and/or "git commit -a")
```

`git diff` で差分に問題がないことを確認したら、カレントディレクトリの内容をすべてリポジトリに追加してコミットします。
```sh
$ git diff

$ git add .

$ git commit -m "編集画面を作成"
[06_update_user 9ccb539] 編集画面を作成
 6 files changed, 100 insertions(+), 5 deletions(-)
 create mode 100644 src/main/resources/templates/users/update.html
 
$ git status
# On branch 06_update_user
nothing to commit (working directory clean)
```

最後に、`master` ブランチにマージして作業用ブランチを削除します。
```sh
$ git checkout master
Switched to branch 'master'

$ git merge 06_update_user 
Updating 1d91a3e..9ccb539
Fast-forward
 src/main/java/sample/app/users/UserForm.java                |   11 +++++++++--
 src/main/java/sample/app/users/UsersController.java         |   34 +++++++++++++++++++++++++++++++++-
 src/main/java/sample/domain/repository/UsersRepository.java |    1 +
 src/main/java/sample/domain/service/users/UsersService.java |   23 +++++++++++++++++++++--
 src/main/resources/templates/users/index.html               |    2 ++
 src/main/resources/templates/users/update.html              |   34 ++++++++++++++++++++++++++++++++++
 6 files changed, 100 insertions(+), 5 deletions(-)
 create mode 100644 src/main/resources/templates/users/update.html

$ git branch -d 06_update_user 
Deleted branch 06_update_user (was 9ccb539).
```


---
続いてユーザ削除画面を作成しましょう。

- [ユーザ削除](./07_delete_user.md)
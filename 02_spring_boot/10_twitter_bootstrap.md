Twitter Bootstrap
=====

この章では [Twitter Bootstrap](http://getbootstrap.com/) を使って画面デザインを改善します。

Twitter Bootstrap は Twitter 社が OSS として公開している CSS フレームワークです。Twitter Bootstrap を活用することで、以下の効果が期待できます。

- 最小限の手間で充分なデザインを提供する
- ブラウザの差異を吸収する
- デバイスの差異を吸収する


---
## ブランチの作成

これまで同様、作業用ブランチを作成して移動します。

```sh
$ git branch
* master

$ git checkout -b 10_twitter_bootstrap
Switched to a new branch '10_twitter_bootstrap'

$ git branch
* 10_twitter_bootstrap
  master
```


---
## 依存性の追加

まず、Twitter Bootstrap を取り込みます。Web サイトや CDN から取得することもできますが、ここでは [WebJars](http://www.webjars.org/) を使います。

`pom.xml` に以下の内容を追記してください。

```xml
<?xml version="1.0" encoding="UTF-8"?>
(snip)
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>bootstrap</artifactId>
			<version>3.3.7-1</version>
		</dependency>
	</dependencies>
(sjip)
</xml>
```

パッケージエクスプローラの `Maven Dependencies` を展開して、`bootstrap` と、Twitter Bootstrap が依存する `jquery` が追加されていることを確認してください。   
![](images/10_001.JPG)  
  

## スタイルシートの適用

依存性に追加した Twitter Bootstrap を適用します。`src/main/resources/templates/layout.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout">
(snip)
<link rel="stylesheet" href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"/> <!-- (10-1) -->
</head>
(snip)
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-1| `<link rel="stylesheet" href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"/>` | 依存性に追加した Twitter Bootstrap の圧縮した css を適用する |

この状態で http://localhost:8080/users/ を再描画して、Twitter Bootstrap の CSS が適用されていることを確認してください。  
![](images/10_002.JPG)  
  

## コンテナ

Twitter Bootstrap には画面内容を包み込んで全体的なデザインを提供する [コンテナ](http://getbootstrap.com/css/#overview-container) という概念があり、2 種類のコンテナが用意されています。

1. `container`
  - 固定幅のコンテナ
2. `container-fluid`
  - 可変幅のコンテナ

ここでは固定幅のコンテナを適用します。`src/main/resources/templates/layout.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
	<div class="container"> <!-- (10-2) -->
		<div layout:fragment="content"></div>
		<footer>
			<p>&copy; 2017 Company, Inc.</p>
		</footer>
	</div>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-2| `<div class="container">` | Thymeleaf でレイアウトする各画面の内容と footer を固定幅で表示する |

この状態で http://localhost:8080/users/ を再描画して、画面の内容が固定幅中央寄せで表示されることを確認してください。  
![](images/10_003.JPG)  
  

## ナビゲーションバー

[ナビゲーションバー](http://getbootstrap.com/components/#navbar) は、アプリケーションロゴや各画面へ遷移するメニューなどを提供する共通ヘッダーです。

Twitter Bootstrap は数種類のナビゲーションバーを提供しています。ここでは常に画面上部に表示されるナビゲーションバーを使いましょう。また、見た目の白黒を反転します。

`src/main/resources/templates/layout.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top"> <!-- (10-3) -->
	<div class="container"> <!-- (10-4) -->
	<div class="navbar-header"> <!-- (10-5) -->
		<a class="navbar-brand" th:href="@{/users/}">ユーザ管理</a> <!-- (10-6) -->
	</div>
	<div class="collapse navbar-collapse"> <!-- (10-7) -->
		<ul class="nav navbar-nav"> <!-- (10-8) -->
(snip)
		</ul>
	</div>
	</div>
	</nav>
	<div class="container"> <!-- (10-2) -->
(snip)
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-3| `<nav class="navbar navbar-inverse navbar-fixed-top">` | HTML5 の `nav` タグでナビゲーション要素であること示す。`navbar` でナビゲーションバー共通のスタイルを適用し、`navbar-inverse` で見た目の白黒を反転する。`navbar-fixed-top` で画面上部に固定する |
|10-4| `<div class="container">` | ナビゲーションバーの内容を固定幅、中央寄せで表示する |
|10-5| `<div class="navbar-header">` | アプリケーションロゴを配置する領域を示す |
|10-6| `<a class="navbar-brand"` | アンカーにアプリケーションロゴのスタイルを適用する |
|10-7| `<div class="collapse navbar-collapse"> ` | ナビゲーションメニューを配置する領域を示す |
|10-8| `<ul class="nav navbar-nav">` | リストにナビゲーションメニューのスタイルを適用する |

この状態で http://localhost:8080/users/ を再描画して、ナビゲーションバーが表示されること、画面上部にナビゲーションバーを固定したために画面内容の上部がナビゲーションバーの裏側に回り込んでしまっていることを確認してください。  
![](images/10_004.JPG)  
  

## スタイルシート

Twitter Bootstrap だけでは対応できない問題に対処する場合や、Twitter Bootstrap が提供するスタイルをカスタマイズする場合は、アプリケーション独自のスタイルシートを使います。

`src/main/resources/templates/layout.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
<link rel="stylesheet" href="/stylesheets/style.css"/> <!-- (10-9) -->
</head>
<body>
(snip)
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-9| `<link rel="stylesheet" href="/stylesheets/style.css"/>` | アプリケーション独自のスタイルシートを追加する |


以下の内容で `src/main/resources/static/stylesheets/style.css` を作成してください。

```css
@CHARSET "UTF-8";
body {
  padding-top: 50px; // (10-1)
}
footer {
  padding: 30px 0; // (10-2)
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-1| `padding-top: 50px;` | 画面内容がナビゲーションバーの裏側に回り込まないよう、画面上部に 50px の余白を設ける |
|10-2| `padding: 30px 0;` | footer の上下に 30px の余白を設ける |

この状態で http://localhost:8080/users/ を再描画して、アプリケーション独自のスタイルシートが適用されていることを確認してください。  
![](images/10_005.JPG)  
  

ここまでで、全画面に共通するデザインを改善しました。


## ページヘッダー

以下、ユーザ一覧画面を例にデザインを改善します。

まず、各画面のタイトルテキストに [ページヘッダー](http://getbootstrap.com/components/#page-header) スタイルを適用します。

`src/main/resources/templates/users/index.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
<div layout:fragment="content">
	<div class="page-header"> <!-- (10-1) -->
	<h1>ユーザ一覧</h1>
	</div>
(snip)
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-1| `<div class="page-header">` | ページヘッダーを配置する領域を示す |

この状態で http://localhost:8080/users/ を再描画して、タイトルテキストにページヘッダーのスタイルが適用されていることを確認してください。  
![](images/10_006.JPG)  
  

## フォーム

続いて、[フォーム](http://getbootstrap.com/css/#forms) のデザインを改善します。

Twitter Bootstrap は数種類のフォームデザインを提供しています。ここでは、ラベルと入力フィールドのペアを並べて表示する [Horizontal form](http://getbootstrap.com/css/#forms-horizontal) を使います。

また、画面を 12 列に分割して各コンポーネントを配置する [Grid System](http://getbootstrap.com/css/#grid) を使います。

`src/main/resources/templates/users/index.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
	<form th:action="@{/users/search}" method="get" th:object="${userForm}"
		class="form-horizontal"> <!-- (10-2) -->
		<div class="form-group"> <!-- (10-3) -->
			<label for="name" class="control-label col-md-2">名前:</label> <!-- (10-4) -->
			<div class="col-md-3"> <!-- (10-5) -->
			<input type="text" th:field="*{name}" class="form-control"/> <!-- (10-6) -->
			</div>
		</div>
		<div class="form-group">
			<label for="email" class="control-label col-md-2">メールアドレス:</label>
			<div class="col-md-3">
			<input type="text" th:field="*{email}" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-offset-2 col-md-1"> <!-- (10-7) -->
			<button>検索</button>
			</div>
		</div>
	</form>
(snip)
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-2| `class="form-horizontal"` | ラベルと入力フィールドのペアを横に並べる |
|10-3| `class="form-group"` | ラベルと入力フィールドのペアを配置する領域を示す。以下同様 |
|10-4| `class="control-label col-md-2"` | スタイルを適用するラベルであること、12 列に分割した画面のうち 2 列分の幅を占めることを示す |
|10-5| `class="col-md-3"` | 12 列に分割した画面のうち 3 列分の幅を占めることを示す |
|10-6| `class="form-control"` | スタイルを適用する入力フィールドであることを示す |
|10-7| `class="col-md-offset-2 col-md-1"` | 12 列に分割した画面のうち 2 列分の余白を設けること、1 列分の幅を占めることを示す |

この状態で http://localhost:8080/users/ を再描画して、フォームにスタイルが適用されていることを確認してください。  
![](images/10_007.JPG)  
  

## テーブル

続いて [テーブル](http://getbootstrap.com/css/#tables) のデザインを改善します。

Twitter Bootstrap は数種類のフォームデザインを提供しています。ここでは、行ごとに背景色を塗り分ける [Striped rows](http://getbootstrap.com/css/#tables-striped) を使います。

`src/main/resources/templates/users/index.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
	</form>
	<table class="table table-striped"> <!-- (10-8) -->
(snip)
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-8| `class="table table-striped"` | スタイルを適用するテーブルであること、行ごとに背景色を塗り分けることを示す |

この状態で http://localhost:8080/users/ を再描画して、テーブルにスタイルが適用されていることを確認してください。  
![](images/10_008.JPG)  
  

## ボタン

続いて [ボタン](http://getbootstrap.com/css/#buttons) のデザインを改善します。

Twitter Bootstrap は様々な色や大きさのボタンを提供しています。また、アンカーを `<button>` のように表示することができます。

`src/main/resources/templates/users/index.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
		<div class="form-group">
			<div class="col-md-offset-2 col-md-1">
			<button class="btn btn-default">検索</button> <!-- (10-9) -->
			</div>
		</div>
(snip)
			<tr th:each="user : ${users}">
				<td th:text="${user.id}"></td>
				<td th:text="${user.name}"></td>
				<td th:text="${user.email}"></td>
				<td><a class="btn btn-default" th:href="@{/users/update(id=${user.id})}">更新</a></td> <!-- (10-10) -->
				<td><a class="btn btn-danger" th:href="@{/users/delete(id=${user.id})}">削除</a></td> <!-- (10-11) -->
			</tr>
		</tbody>
	</table>
	<a class="btn btn-primary" th:href="@{/users/create}">新規作成</a> <!-- (10-12) -->
</div>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-9| `button class="btn btn-default"` | 通常の大きさ、色のボタンであることを示す |
|10-10| `a class="btn btn-default"` | アンカーを通常の大きさ、色のボタンとして表示することを示す |
|10-11| `a class="btn btn-danger"`` | アンカーを通常の大きさ、注意を促す色のボタンとして表示することを示す |
|10-12| `a class="btn btn-primary"` | アンカーを通常の大きさ、主要な操作を促す色のボタンとして表示することを示す |

この状態で http://localhost:8080/users/ を再描画して、ボタンとアンカーにスタイルが適用されていることを確認してください。  
![](images/10_009.JPG)  
  

## アラート

最後に [メッセージ表示欄](http://getbootstrap.com/components/#alerts) のデザインを改善します。

`src/main/resources/templates/users/index.html` に以下の内容を追記してください。

```html
<!DOCTYPE html>
(snip)
	<div th:if="${success}" class="alert alert-success">Success!</div> <!-- (10-13) -->
(snip)
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|10-13| `class="alert alert-success"` | 正常処理メッセージを表示する領域であることを示す |

この状態で http://localhost:8080/users/ に再度アクセスしてユーザを登録し、処理完了メッセージにスタイルが適用されていることを確認してください。  
![](images/10_010.JPG)  
![](images/10_011.JPG)  
  

## その他の画面への適用

ここまでの内容を参考に、下記各画面のデザインも改善してください。

- トップ画面
- ユーザ登録画面
- ユーザ編集画面
- ユーザ削除画面


---
## コミットとマージ

以上で Twitter Bootstrap の適用は完了です。ここまでの作業内容を git に保存しましょう。

`git status` コマンドでブランチの状態を確認します。
```sh
$ git status
On branch 10_twitter_bootstrap
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   pom.xml
        modified:   src/main/resources/templates/index.html
        modified:   src/main/resources/templates/layout.html
        modified:   src/main/resources/templates/users/create.html
        modified:   src/main/resources/templates/users/delete.html
        modified:   src/main/resources/templates/users/index.html
        modified:   src/main/resources/templates/users/update.html

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        src/main/resources/static/

no changes added to commit (use "git add" and/or "git commit -a")
```

`git diff` で差分に問題がないことを確認したら、カレントディレクトリの内容をすべてリポジトリに追加してコミットします。
```sh
$ git diff

$ git add .

$ git commit -m "Twitter Bootstrap を適用"
[10_twitter_bootstrap 451daf4] Twitter Bootstrap を適用
 8 files changed, 144 insertions(+), 80 deletions(-)
 create mode 100644 src/main/resources/static/stylesheets/style.css
 rewrite src/main/resources/templates/users/create.html (62%)

$ git status
On branch 10_twitter_bootstrap
nothing to commit, working tree clean
```

最後に、`master` ブランチにマージして作業用ブランチを削除します。
```sh
$ git checkout master
Switched to branch 'master'

$ git merge 10_twitter_bootstrap
Updating 2c3e13b..451daf4
Fast-forward
 pom.xml                                         |  5 +++
 src/main/resources/static/stylesheets/style.css |  7 ++++
 src/main/resources/templates/index.html         |  4 ++-
 src/main/resources/templates/layout.html        | 16 ++++++---
 src/main/resources/templates/users/create.html  | 45 ++++++++++++++++---------
 src/main/resources/templates/users/delete.html  | 27 ++++++++++-----
 src/main/resources/templates/users/index.html   | 37 ++++++++++++--------
 src/main/resources/templates/users/update.html  | 41 ++++++++++++++--------
 8 files changed, 123 insertions(+), 59 deletions(-)
 create mode 100644 src/main/resources/static/stylesheets/style.css

$ git branch -d 10_twitter_bootstrap
Deleted branch 10_twitter_bootstrap (was 451daf4).

$ git branch
* master
```

---
続いて各種メッセージを改善しましょう。

- [メッセージ](./11_messages.md)
ユーザ一覧画面の作成
=====

この章では以下の内容を学習します。

- インメモリデータベースの初期化
- エンティティの作成
- リポジトリの作成
- サービスの作成
- Thyemeleaf によるコレクションの表示

---
## ブランチの作成

これまで同様、作業用ブランチを作成して移動します。

[トップ画面](./03_hello_spring_boot.md) を作成した際はブランチを作ってから移動しましたが、`git checkout` コマンドに `-b` をオプションを指定することでブランチの作成と移動を一度に実行できます。
```sh
$ git branch
* master

$ git checkout -b 04_show_users
Switched to a new branch '04_show_users'

$ git branch
* 04_show_users
  master
```

---
## インメモリデータベースの初期化

本資料では依存性に `H2` を追加しているので、アプリケーションの起動に合わせて H2 インメモリデータベースが起動し、このデータベースに対するデータソースが自動的に設定されます。

また、依存性に `JPA` を追加しているので Spring JDBC によるデータベース初期化機能が有効になっています。Spring JDBC はクラスパスのルートに `schema.sql` および `data.sql` が存在する場合、これらの内容をデータベースに適用します。この機能を利用してインメモリデータベースを初期化しましょう。

まず、以下のとおり `users` テーブルを作成する DDL を `src/main/resources/schema.sql` に作成してください。

```sql
drop table if exists users;

create table users
(
  id integer primary key auto_increment,
  name varchar(60) not null,
  email varchar(60) not null,
  password varchar(60) not null
)
```

続いて、作成した `users` テーブルにデータを投入する DML を `src/main/resources/data.sql` に作成します。
```sql
insert into users (name, email, password) values 
	('ユーザ1', 'user1@example.com', 'dummy'),
	('ユーザ2', 'user2@example.com', 'dummy'),
	('ユーザ3', 'user3@example.com', 'dummy');
```

ところで、JPA (Hibernate) にはエンティティクラスの内容からテーブルを自動生成する機能があり、こちらもデフォルトで有効になっています。この機能が有効になっていると、上記の DDL および DML で作成した内容が DROP されてしまうため、`src/main/resources/application.properties` に以下の設定を追記して無効化します。

```
spring.jpa.hibernate.ddl-auto=none
```

併せて、Hibernate が発行する SQL をコンソールログに出力する設定も追記しましょう。

```
spring.jpa.properties.hibernate.format_sql=true

logging.level.org.hibernate.SQL=debug
logging.level.org.hibernate.type.descriptor.sql.BasicBinder=trace
```

`application.properties` に設定できる項目は下記リンクを参照してください。
- [Appendix A\. Common application properties](http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)

以上でインメモリデータベースの初期化は完了です。


## エンティティの作成

次に、作成した users テーブルに対応するエンティティクラスを `src/main/java/sample/domain/model/User.java` に作成します。

```java
package sample.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity // (4-1)
@Table(name = "users") // (4-2)
@Data // (4-3)
public class User {

	@Id // (4-4)
	@GeneratedValue // (4-5)
	private Integer id;

	private String name;
	private String email;
	private String password;
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|4-1| `@Entity` | エンティティクラスであることを示す。Spring コンテナに登録される |
|4-2| `@Table(name = "users")` | `users` テーブルに対応することを示す。エンティティとテーブルが同じ名前の場合は省略可能 |
|4-3| `@Data` | Lombok のアノテーション。Getter/Setter などを自動生成する |
|4-4| `@Id` | 当該フィールドに対応する列が主キーであることを示す |
|4-5| `@GeneratedValue` | 当該列の値が自動生成されることを示す |

STS のアウトラインビューに Lombok が生成した各種メソッドが表示されることを確認しましょう。  
![](images/04_001.JPG)


## リポジトリの作成

続いて、作成した users テーブルにアクセスするリポジトリインタフェースを `src/main/java/sample/domain/repsository/UsersRepository.java` に作成します。

```java
package sample.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sample.domain.model.User;

public interface UsersRepository extends JpaRepository<User, Integer> { // (4-1), (4-2)
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|4-1| `extends JpaRepository` | `JpaRepository` インタフェースを継承する。このインタフェースを実装するプロキシクラスが生成されて Spring コンテナに登録される |
|4-2| `<User, Integer>` | アクセスするエンティティが `User` クラスで主キーが `Integer` 型であることを示す |

ここでは `JpaRepository` に用意されているメソッドを利用するため、メソッドは特に宣言しません。


## サービスの作成

さらに、作成したリポジトリインタフェースを利用して業務処理を行うサービスクラスを `src/main/java/sample/domain/service/users/UsersService.java` に作成します。

```java
package sample.domain.service.users;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import sample.domain.model.User;
import sample.domain.repository.UsersRepository;

@Service // (4-1)
@AllArgsConstructor // (4-2)
@Transactional // (4-3)
public class UsersService {

	private final UsersRepository usersRepository; // (4-4)

	@Transactional(readOnly = true) // (4-5)
	public List<User> findAll() {
		return usersRepository.findAll();
	}
}
```

各記述内容の詳細は以下のとおり。

|項番|内容|詳細|
|-|-|-|
|4-1| `@Service ` | サービスクラスであることを示す。Spring コンテナに登録される |
|4-2| `@AllArgsConstructor` | コンストラクタインジェクションに使用するコンストラクタを自動生成する |
|4-3| `@Transactional` | サービスクラスのすべてのメソッドがトランザクション境界であることを示す |
|4-4| `private final UsersRepository usersRepository` | コンストラクタインジェクションによってインスタンスをインジェクションするフィールドを宣言する。`final` の指定は必須ではなく推奨 |
|4-5| `@Transactional(readonly = true)` | 検索処理のトランザクションは readonly とする |

ここでは、JpaRepository が用意している findAll の結果をそのまま返します。

以上でドメイン層の実装は完了です。


## コントローラの作成

ここからはアプリケーション層を実装します。

まず、作成したサービスクラスを利用してテーブルから取得したデータを返却するようコントローラを `src/main/java/sample/app/users/UsersController.java` に作成してください。

```java
package sample.app.users;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import lombok.AllArgsConstructor;
import sample.domain.model.User;
import sample.domain.service.users.UsersService;

@Controller
@RequestMapping("users")
@AllArgsConstructor
public class UsersController {

	private final UsersService usersService;

	@GetMapping
	public String index(Model model) {
		List<User> users = usersService.findAll();
		model.addAttribute("users", users);
		return "users/index";
	}
}
```

既に説明した内容を除いて、特筆すべき内容はありません。


## ビューテンプレートの作成

続いて、コントローラから渡されたモデルの内容を表示するビューテンプレートを `src/main/resources/templates/users/index.html` に作成しましょう。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
<head>
<meta charset="UTF-8" />
<title>一覧</title>
</head>
<body>
	<h1>ユーザ一覧</h1>
	<table>
		<thead>
			<tr>
				<th>id</th>
				<th>名前</th>
				<th>メールアドレス</th>
			</tr>
		</thead>
		<tbody>
			<tr th:each="user : ${users}"> <!-- (4-1) -->
				<td th:text="${user.id}"></td> <!-- (4-2) -->
				<td th:text="${user.name}"></td>
				<td th:text="${user.email}"></td>
			</tr>
		</tbody>
	</table>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|4-1| `th:each="user : ${users}"` | モデルから `users` という名前のユーザ情報を取得し、一件ずつ `user` という名前で処理する |
|4-2| `th:text="${user.id}"` | `user` の `id` 属性の値でタグの内容を置き換える。以下同様 |


## トップ画面の変更

最後に作成したユーザ一覧画面へ遷移するアンカーをトップ画面に追加しましょう。

```html
	(snip)
	<h1>Hello, Spring Boot!</h1>
	<div th:text="${now}"></div>
	<a href="users">ユーザ一覧</a>
</body>
</html>
```

再び http://localhost:8080/ にアクセスし、ユーザ一覧画面に遷移するアンカーが表示されることを確認します。   
![](images/04_002.JPG)  

続いて当該アンカーをクリックし、ユーザ一覧画面に初期データスクリプトで登録した内容が表示されることを確認してください。   
![](images/04_003.JPG)  

併せて Hibernate が発行した SQL のログがコンソールに出力されていることも確認しておきましょう。
```
2017-04-01 15:34:28.606 DEBUG 88112 --- [nio-8080-exec-2] org.hibernate.SQL                        : 
    select
        user0_.id as id1_0_,
        user0_.email as email2_0_,
        user0_.name as name3_0_,
        user0_.password as password4_0_ 
    from
        users user0_
```


---
## コミットとマージ

ここまでの作業内容を git に保存しましょう。

`git status` コマンドでブランチの状態を確認します。
```sh
$ git status
# On branch 04_show_users
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   src/main/resources/application.properties
#	modified:   src/main/resources/templates/index.html
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/main/java/sample/app/users/
#	src/main/java/sample/domain/
#	src/main/resources/data.sql
#	src/main/resources/schema.sql
#	src/main/resources/templates/users/
no changes added to commit (use "git add" and/or "git commit -a")
```

バージョン管理下にあるファイルが変更されたこと、バージョン管理下にないファイルやディレクトリが追加されたことが分かります。

`git diff` コマンドでバージョン管理下にあるファイルの変更内容を確認しましょう。
```sh
$ git diff
diff --git a/src/main/resources/application.properties b/src/main/resources/application.properties
index e69de29..164eb60 100644
--- a/src/main/resources/application.properties
+++ b/src/main/resources/application.properties
@@ -0,0 +1,6 @@
+spring.jpa.hibernate.ddl-auto=none
+
+spring.jpa.properties.hibernate.format_sql=true
+
+logging.level.org.hibernate.SQL=debug
+logging.level.org.hibernate.type.descriptor.sql.BasicBinder=trace
\ No newline at end of file
diff --git a/src/main/resources/templates/index.html b/src/main/resources/templates/index.html
index 3b14469..8add622 100644
--- a/src/main/resources/templates/index.html
+++ b/src/main/resources/templates/index.html
@@ -8,5 +8,6 @@
 <body>
        <h1>Hello, Spring Boot!</h1>
        <div th:text="${now}"></div> <!-- (3-2) -->
+       <a href="users">ユーザ一覧</a>
 </body>
 </html>
\ No newline at end of file
```

これまでと同様に、カレントディレクトリの内容をすべてリポジトリに追加してコミットします。
```sh
$ git add .

$ git status
# On branch 04_show_users
# Changes to be committed:
#   (use "git reset HEAD <file>..." to unstage)
#
#	new file:   src/main/java/sample/app/users/UsersController.java
#	new file:   src/main/java/sample/domain/model/User.java
#	new file:   src/main/java/sample/domain/repository/UsersRepository.java
#	new file:   src/main/java/sample/domain/service/users/UsersService.java
#	modified:   src/main/resources/application.properties
#	new file:   src/main/resources/data.sql
#	new file:   src/main/resources/schema.sql
#	modified:   src/main/resources/templates/index.html
#	new file:   src/main/resources/templates/users/index.html
#

$ git commit -m "一覧画面を作成"
[04_show_users aa5bc7f] 一覧画面を作成
 9 files changed, 126 insertions(+)
 create mode 100644 src/main/java/sample/app/users/UsersController.java
 create mode 100644 src/main/java/sample/domain/model/User.java
 create mode 100644 src/main/java/sample/domain/repository/UsersRepository.java
 create mode 100644 src/main/java/sample/domain/service/users/UsersService.java
 create mode 100644 src/main/resources/data.sql
 create mode 100644 src/main/resources/schema.sql
 create mode 100644 src/main/resources/templates/users/index.html
```

最後に、`master` ブランチにマージして作業用ブランチを削除します。
```sh
$ git checkout master
Switched to branch 'master'

$ git merge 04_show_users 
Updating 24a9cde..aa5bc7f
Fast-forward
 src/main/java/sample/app/users/UsersController.java         |   27 +++++++++++++++++++++++++++
 src/main/java/sample/domain/model/User.java                 |   22 ++++++++++++++++++++++
 src/main/java/sample/domain/repository/UsersRepository.java |    8 ++++++++
 src/main/java/sample/domain/service/users/UsersService.java |   22 ++++++++++++++++++++++
 src/main/resources/application.properties                   |    6 ++++++
 src/main/resources/data.sql                                 |    4 ++++
 src/main/resources/schema.sql                               |    9 +++++++++
 src/main/resources/templates/index.html                     |    1 +
 src/main/resources/templates/users/index.html               |   27 +++++++++++++++++++++++++++
 9 files changed, 126 insertions(+)
 create mode 100644 src/main/java/sample/app/users/UsersController.java
 create mode 100644 src/main/java/sample/domain/model/User.java
 create mode 100644 src/main/java/sample/domain/repository/UsersRepository.java
 create mode 100644 src/main/java/sample/domain/service/users/UsersService.java
 create mode 100644 src/main/resources/data.sql
 create mode 100644 src/main/resources/schema.sql
 create mode 100644 src/main/resources/templates/users/index.html

$ git branch -d 04_show_users 
Deleted branch 04_show_users (was aa5bc7f).

$ git branch
* master
```

---
以上でユーザ一覧画面の作成は完了です。続いてユーザ登録画面を作成しましょう。

- [ユーザ登録](./05_create_user.md)
トップ画面の作成
=====

この章では以下の内容を学習します。

- Spring MVC を使ったコントローラの作成
- Thymeleaf を使ったビューテンプレートの作成
- Spring Boot アプリケーションの起動と停止

---
## ブランチの作成

ここからは章ごとに作業用のブランチを作成します。まず `git branch` コマンドでブランチの状態を確認します。
```sh
$ git branch
* master
```

`master` ブランチがひとつだけあることが分かります。

続いて `git branch` コマンドにブランチ名を渡してブランチを作成します。以下の例では本資料の名称と合わせていますが、任意の名前で構いません。
```sh
$ git branch 03_hello_spring_boot
```

先ほどと同様にブランチの状態を確認します。
```sh
$ git branch
  03_hello_spring_boot
* master
```

作業用ブランチが作成されたことが分かります。

最後に `git checkout` コマンドで作成したブランチに移動します。
```sh
$ git checkout 03_hello_spring_boot 
Switched to branch '03_hello_spring_boot'
```

作業用ブランチに移動したことを確認します。
```sh
$ git branch
* 03_hello_spring_boot
  master
```

以上で git ブランチの作成は完了です。

---
## コントローラの作成

それでは実際にトップ画面を作成しましょう。

まず、コンテキストルート `/` への `GET` リクエストを処理するコントローラを作ります。以下の内容で `src/main/java/sample/app/IndexController.java` を作成してください。

```java
package sample.app;

import java.time.LocalDateTime;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller // (3-1)
@RequestMapping("/") // (3-2)
public class IndexController {

	@GetMapping // (3-3)
	public String index(Model model) { // (3-4)
		model.addAttribute("now", LocalDateTime.now()); // (3-5)
		return "index"; // (3-6)
	}
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|3-1| `@Controller` | コントローラクラスであることを示す。Spring コンテナに登録される |
|3-2| `@RequestMapping("/")` | URL "/" に対応するアクセスを処理することを示す |
|3-3| `@GetMapping` | GET メソッドを処理することを示す |
|3-4| `Model model` | ビューが参照するモデルを変数名 `model` で引数として受け取る |
|3-5| `model.addAttribute("now", LocalDateTime.now())` | 現在日時を属性名 "now" でモデルに追加する |
|3-6| `return "index"` | ビューテンプレートとして `src/main/resource/index.html` を返却することを示す |


## ビューテンプレートの作成

続いて、コントローラで指定したビューテンプレート `src/main/resources/templates/index.html` を以下の内容で作成してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/"> <!-- (3-1) -->
<head>
<meta charset="UTF-8" />
<title>トップ</title>
</head>
<body>
	<h1>Hello, Spring Boot!</h1>
	<div th:text="${now}"></div> <!-- (3-2) -->
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|3-1| `xmlns:th="http://www.thymeleaf.org/"` | thymeleaf を利用するため XML 名前空間を指定する |
|3-2| `th:text="${now}"` | コントローラで model に追加した "now" の値を表示する |

Thymeleaf はビューテンプレートを XML として解釈するため、`meta` タグを `/>` で終了する点に注意してください。


## アプリケーションの起動

それでは作成した Spring Boot アプリケーションを起動しましょう。

Spring Boot アプリケーションは以下に示すようないくつかの方法で起動することができます。

- STS などの IDE から起動
- 実行可能 JAR ファイルにパッケージして起動
- maven プラグインから起動

ここでは STS から起動する方法を紹介します。その他の方法については以下のドキュメントを参照してください。

- [19. Running your application](http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html)

パッケージエクスプローラで `src/main/java/sample/SampleApplication.java` を選択したら、メニューバーの実行ボタンから `Run As` -> `Spring Boot App` を選択してください。  
![](images/03_001.JPG)  

組み込み Tomcat が 8080 番ポートで正常に起動すると、コンソールに以下のログが表示されます。
```sh
2017-04-01 12:41:46.369  INFO 86125 --- [  restartedMain] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8080 (http)
2017-04-01 12:41:46.385  INFO 86125 --- [  restartedMain] sample.SampleApplication                 : Started SampleApplication in 19.644 seconds (JVM running for 21.866)
```

それではブラウザから http://localhost:8080/ にアクセスして、作成したビューテンプレートの内容が表示されることを確認しましょう。  
![](images/03_002.JPG)  
  

画面をリフレッシュして時刻が動的に更新されることを確認してください。  
![](images/03_003.JPG)  
  

アプリケーションを停止する場合は、メニューバーの停止ボタンをクリックします。  
![](images/03_004.JPG)  
  

なお、本資料では依存性に `DevTools (spring-boot-devtools)` を追加しているので、アプリケーションを起動した状態でソースコードを変更すると自動的にアプリケーションが再起動されます。

以降の章では、この機能を利用して開発を進めます。アプリケーションを停止した場合は再度起動してください。

---
## コミットとマージ

ここまでの作業内容を git に保存しましょう。

まず `git status` コマンドでブランチの状態を確認します。
```sh
$ git status
# On branch 03_hello_spring_boot
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/main/java/sample/app/
#	src/main/resources/templates/
nothing added to commit but untracked files present (use "git add" to track)
```

作成した 2 ファイルはバージョン管理されていないことが分かります。`git add .` コマンドでカレントディレクトリ配下をすべてリポジトリに追加してください。
```sh
$ git add .
```

作成した 2 ファイルがリポジトリに追加されたことを確認します。
```sh
$ git status
# On branch 03_hello_spring_boot
# Changes to be committed:
#   (use "git reset HEAD <file>..." to unstage)
#
#	new file:   src/main/java/sample/app/IndexController.java
#	new file:   src/main/resources/templates/index.html
#
```

適当なメッセージを渡してコミットします。
```sh
$ git commit -m "トップ画面を作成"
[03_hello_spring_boot 24a9cde] トップ画面を作成
 2 files changed, 31 insertions(+)
 create mode 100644 src/main/java/sample/app/IndexController.java
 create mode 100644 src/main/resources/templates/index.html
```

作業ディレクトリの内容がリポジトリに反映されたことを確認します。
```sh
$ git status
# On branch 03_hello_spring_boot
nothing to commit (working directory clean)
```

最後に、作業用ブランチの内容を `master` ブランチにマージします。`git checkout` コマンドで `master` ブランチに移動してください。
```sh
$ git checkout master
Switched to branch 'master'

$ git branch
  03_hello_spring_boot
* master
```

`git merge` コマンドで作業用ブランチの内容をマージします。
```sh
$ git merge 03_hello_spring_boot 
Updating 2c1462c..24a9cde
Fast-forward
 src/main/java/sample/app/IndexController.java |   19 +++++++++++++++++++
 src/main/resources/templates/index.html       |   12 ++++++++++++
 2 files changed, 31 insertions(+)
 create mode 100644 src/main/java/sample/app/IndexController.java
 create mode 100644 src/main/resources/templates/index.html
```

`git log` コマンドで履歴を確認してみましょう。
```sh
$ git log
```

以下のような履歴画面が表示されます。履歴を確認したら `q` で履歴画面を閉じてください。
```sh
commit 24a9cde4412a82d2d41d7a9bf3b0d55b3a0f67a6
Author: garbagetown <garbagetown@gmail.com>
Date:   Sat Apr 1 12:49:19 2017 +0900

    トップ画面を作成

commit 2c1462c4d1cec83d940d74a2df33455890c5b8a9
Author: garbagetown <garbagetown@gmail.com>
Date:   Sat Apr 1 12:16:14 2017 +0900

    プロジェクトを作成
(END)
```

最後に、不要になった作業用ブランチを削除しましょう。`git branch` コマンドに削除オプション `-d` とブランチ名を指定してください。
```sh
$ git branch -d 03_hello_spring_boot 
Deleted branch 03_hello_spring_boot (was 24a9cde).
```

ブランチが削除されたことを確認します。
```sh
$ git branch
* master
```
---

以上でトップ画面の作成は完了です。続いて一覧画面を作成しましょう。

- [ユーザ一覧](./04_show_users.md)
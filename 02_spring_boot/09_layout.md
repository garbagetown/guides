レイアウト
=====

この章では Thymeleaf によるレイアウト方法を紹介します。Thymeleaf では以下のレイアウト方法を提供しています。

- Include-style レイアウト
  - [Thymeleaf Standard Layout System](http://www.thymeleaf.org/doc/articles/layouts.html#thymeleaf-standard-layout-system)
- Hierarchical-style レイアウト
  - [Thymeleaf Tiles Integration](http://www.thymeleaf.org/doc/articles/layouts.html#thymeleaf-tiles-integration)
  - [Thymeleaf Layout Dialect](thymeleaf-layout-dialect)

この章では Thymeleaf Layout Dialect によるレイアウトを紹介します。その他については上記リンクを参照してください。

- [Thymeleaf Page Layouts - Thymeleaf](http://www.thymeleaf.org/doc/articles/layouts.html)

なお、本資料では `Thymeleaf (spring-boot-starter-thymeleaf)` を依存性に追加しているので、必要な依存性や Bean は設定済みです。


---
## ブランチの作成

これまで同様、作業用ブランチを作成して移動します。

```sh
$ git branch
* master

$ git checkout -b 09_layout
Switched to a new branch '09_layout'

$ git branch
* 09_layout
  master
```


---
## 共通レイアウトの作成

まず、全画面で共通となるレイアウトを作成します。以下の内容で `src/main/resources/templates/layout.html` を作成します。

```sh
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"> <!-- (9-1) -->
<head>
<meta charset="UTF-8"/>
<title layout:title-pattern="$DECORATOR_TITLE - $CONTENT_TITLE">ユーザ管理</title> <!-- (9-2) -->
</head>
<body>
	<div>
		<a th:href="@{/users/}">ユーザ管理</a>
	</div>
	<div>
		<ul>
			<li><a th:href="@{/users/}">検索</a></li>
			<li><a th:href="@{/users/create}">登録</a></li>
		</ul>
	</div>
	<div>
		<div layout:fragment="content"></div> <!-- (9-3) -->
		<footer>
			<p>&copy; 2017 Company, Inc.</p>
		</footer>
	</div>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|9-1| `xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"` | Thymeleaf Layout Dialect を使うための名前空間 |
|9-2| `layout:title-pattern="$DECORATOR_TITLE - $CONTENT_TITLE"` | `$DECORATOR_TITLE` には共通レイアウト画面の `title` タグの値、`$CONTENT_TITLE` には後述するコンテンツ画面の `title` タグの値が設定される |
|9-3| `layout:fragment="content"` | `content` という名前のフラグメントを設定する |


## トップ画面の変更

次に、作成した共通レイアウトを使うようトップ画面を変更します。`src/main/resources/templates/index.html` に以下の内容を追加してください。
```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/" 
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
	layout:decorator="layout"> <!-- (9-1) -->
<head>
<meta charset="UTF-8" />
<title>トップ</title>
</head>
<body>
<div layout:fragment="content"> <!-- (9-2) -->
	<h1>Hello, Spring Boot!</h1>
(snip)
</div>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|9-1| `layout:decorator="layout"` | 共通レイアウトに作成した `layout.html` を指定する |
|9-2| `<div layout:fragment="content">` | `content` という名前のフラグメントを設定する |


## トップ画面の確認

ブラウザを開いて http://localhost:8080/ にアクセスし、トップ画面に共通レイアウトが適用されていることを確認します。  
![](images/09_001.JPG)  
  

## 一覧画面の変更と確認

同様に、一覧画面を変更します。`src/main/resources/templates/users/index.html` に以下の内容を追加してください。
```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
	layout:decorator="layout">
<head>
<meta charset="UTF-8" />
<title>一覧</title>
</head>
<body>
<div layout:fragment="content">
	<h1>ユーザ一覧</h1>
(snip)
</div>
</body>
</html>
```

ブラウザを開いて http://localhost:8080/users にアクセスし、一覧画面に共通レイアウトが適用されていることを確認します。  
![](images/09_002.JPG)  
  

## 登録画面の変更と確認

同様に、登録画面を変更します。`src/main/resources/templates/users/create.html` に以下の内容を追加してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
	layout:decorator="layout">
<head>
<meta charset="UTF-8" />
<title>登録</title>
</head>
<body>
<div layout:fragment="content">
	<h1>ユーザ登録</h1>
(snip)
</div>
</body>
</html>
```

ブラウザを開いて http://localhost:8080/users/create にアクセスし、登録画面に共通レイアウトが適用されていることを確認します。  
![](images/09_003.JPG)  
  

## 編集画面の変更と確認

同様に、編集画面を変更します。`src/main/resources/templates/users/update.html` に以下の内容を追加してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
	layout:decorator="layout">
<head>
<meta charset="UTF-8" />
<title>更新</title>
</head>
<body>
<div layout:fragment="content">
(snip)
</div>
</body>
</html>
```

ブラウザを開いて http://localhost:8080/users/update にアクセスし、編集画面に共通レイアウトが適用されていることを確認します。  
![](images/09_004.JPG)  
  

## 削除画面の変更と確認

最後に、削除画面を変更します。`src/main/resources/templates/users/delete.html` に以下の内容を追加してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
	layout:decorator="layout">
<head>
<meta charset="UTF-8" />
<title>更新</title>
</head>
<body>
<div layout:fragment="content">
(snip)
</div>
</body>
</html>
```

ブラウザを開いて http://localhost:8080/users/delete にアクセスし、削除画面に共通レイアウトが適用されていることを確認します。  
![](images/09_005.JPG)  
  

---
## コミットとマージ

以上で共通レイアウトの作成は完了です。ここまでの作業内容を git に保存しましょう。

`git status` コマンドでブランチの状態を確認します。
```sh
$ git status
# On branch 09_layout
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   src/main/resources/templates/index.html
#	modified:   src/main/resources/templates/users/create.html
#	modified:   src/main/resources/templates/users/delete.html
#	modified:   src/main/resources/templates/users/index.html
#	modified:   src/main/resources/templates/users/update.html
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/main/resources/templates/layout.html
no changes added to commit (use "git add" and/or "git commit -a")
```

`git diff` で差分に問題がないことを確認したら、カレントディレクトリの内容をすべてリポジトリに追加してコミットします。
```sh
$ git diff

$ git add .

$ git commit -m "共通レイアウトを作成"
[09_layout 42d4ac7] 共通レイアウトを作成
 6 files changed, 51 insertions(+), 5 deletions(-)
 create mode 100644 src/main/resources/templates/layout.html

$ git status
# On branch 09_layout
nothing to commit (working directory clean)
```

最後に、`master` ブランチにマージして作業用ブランチを削除します。
```sh
$ git checkout master
Switched to branch 'master'

$ git merge 09_layout 
Updating ead3ef5..42d4ac7
Fast-forward
 src/main/resources/templates/index.html        |    6 +++++-
 src/main/resources/templates/layout.html       |   26 ++++++++++++++++++++++++++
 src/main/resources/templates/users/create.html |    6 +++++-
 src/main/resources/templates/users/delete.html |    6 +++++-
 src/main/resources/templates/users/index.html  |    6 +++++-
 src/main/resources/templates/users/update.html |    6 +++++-
 6 files changed, 51 insertions(+), 5 deletions(-)
 create mode 100644 src/main/resources/templates/layout.html

$ git branch -d 09_layout 
Deleted branch 09_layout (was 42d4ac7).

$ git branch
* master
```


---
続いて Twitter Bootstrap を使って画面デザインを改善しましょう。

- [Twitter Bootstrap](./10_twitter_bootstrap.md)
はじめに
=====

## 目的

本資料では、Git と Spring Boot を使った Web アプリケーションの開発手順を紹介します。

主な目的は以下のとおりです。

- Spring Boot および関連技術の存在と基本的な使い方を知る
- 必要最低限のコストで実践的なアプリケーションを開発する
- git の使い方に慣れる
  
  
より網羅的な内容については以下の Web サイトおよび書籍を参照してください。

- Web
  - [Spring Boot Reference Guide](http://docs.spring.io/spring-boot/docs/current/reference/html/index.html)
  - [TERASOLUNA Server Framework for Java (5.x) Development Guideline 5.3.0.RELEASE documentation](http://terasolunaorg.github.io/guideline/5.3.0.RELEASE/ja/)
- 書籍
  - [はじめてのSpring Boot](https://www.amazon.co.jp/dp/4777519694)
  - [Spring徹底入門](https://www.amazon.co.jp/dp/4798142476)
  
  
## モジュール構成

利用する主なフレームワーク、ライブラリは以下のとおりです。

![](images/01_001.JPG)
  
  
各モジュールの詳細は下記のとおりです。

|項番|名称|バージョン|機能|
|-|-|-|-|
| 1 | Developer Tools | 1.5.2 | 開発時のみ利用する。リソースの変更を検知してアプリケーションの再起動を行うなど、開発支援機能を提供する |
| 2 | Spring Framework | 4.3.7 | DIやAOPなどの機能を提供する |
| 3 | Spring Boot | 1.5.2 | Spring Frameworkの各種設定の自動化や依存性の解決など、アプリケーション基盤を提供する |
| 4 | Tomcat | 8.5.11 | 開発時のみ利用する(本番環境はAzure WebAppsのTomcatを利用)。組み込みサーブレットコンテナ機能を提供する |
| 5 | Thymeleaf | 2.1.5 | ビューテンプレート機能を提供する |
| 6 | jQuery | 1.11.1 | JavaScriptフレームワーク。DOM操作やAjax通信などの機能を提供する |
| 7 | Twitter Bootstrap | 3.3.7-1 | CSSフレームワーク。レイアウトやコンポーネントなどのスタイルを提供する |
| 8 | Spring Security | 4.2.2 | セキュリティフレームワーク。認証、認可機能を提供する |
| 9 | Lombok | 1.16.14 | コード生成ライブラリ。アクセサ自動生成などの機能を提供する |
| 10 | Spring MVC | 4.3.7 | Webフレームワーク。MVC 機能を提供する |
| 11 | Spring Data JPA (Hibernate) | 1.11.1 (5.0.12) | データベースアクセスフレームワーク。CRUD 機能を提供する |
| 12 | H2 Database | 1.4.193 | 開発時のみ利用する。インメモリデータベースを提供する |
  
  
## ディレクトリ構成

以下にディレクトリ構成を示します。本資料では maven 標準のディレクトリ構成に加えて、レイヤー化アーキテクチャを採用します。

```
.
├── mvnw
├── mvnw.cmd
├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── sample
│   │   │       ├── app
│   │   │       │   ├── xxx
│   │   │       │   │   ├── XxxController.java
│   │   │       │   │   ├── XxxForm.java
│   │   │       │   │   ├── XxxFormValidator.java
│   │   │       │   ├── ...
│   │   │       ├── domain
│   │   │       │   ├── model
│   │   │       │   │   ├── Yyy.java
│   │   │       │   │   ├── ...
│   │   │       │   ├── repository
│   │   │       │   │   ├── YyyRepository.java
│   │   │       │   │   ├── ...
│   │   │       │   └── service
│   │   │       │       ├── xxx
│   │   │       │       │   ├── XxxService.java
│   │   │       │       │   ├── XxxxException.java
│   │   │       │       │   ├── ...
│   │   │       │       ├── ...
│   │   │       ├── util
│   │   │       │   ├── AaaUtil.java
│   │   │       │   ├── ...
│   │   │       ├── XxxApplication.java
│   │   │       ├── XxxConfig.java
│   │   │       ├── ...
│   │   └── resources
│   │       ├── application.properties
│   │       ├── data.sql
│   │       ├── messages.properties
│   │       ├── schema.sql
│   │       ├── static
│   │       │   ├── javascripts
│   │       │   │   ├── xxx.js
│   │       │   │   ├── ...
│   │       │   └── stylesheets
│   │       │       └── layout.css
│   │       └── templates
│   │           ├── xxx
│   │           │   ├── index.html
│   │           │   ├── ...
│   └── test
│       └── java
└── target
    ├── classes
    ├── site
    │   └── apidocs
    └── test-classes
```
  
  
各コンポーネントの依存関係は以下のとおりです。  
![](images/01_002.JPG)
  
  
各ディレクトリに含まれるファイルと内容は以下のとおりです。

|項番|パス|例|内容|
|-|-|-|-|
|1|.|mvnw, mvnw.cmd|maven ラッパースクリプト。システムに mvn がインストールされていない場合、自動的にインストールする|
|2|.|pom.xml|maven 設定ファイル。ライブラリ依存性やビルドに利用するプラグインの設定を記述する|
|3|src/main/java/root|RootApplication, RootConfig|ソースコードのルートディレクトリ。アプリケーションの実行クラス、設定クラスを格納する|
|4|src/main/java/root/app/xxx|XxxController, XxxForm, XxxFormValidator|コントローラ、フォーム、入力チェックなど、画面に関連するクラスを格納する。業務ごとにサブディレクトリを作成する|
|5|src/main/java/root/domain/model|Yyy,Zzz|データベーステーブルに対応するエンティティクラスを格納する|
|6|src/main/java/root/domain/repository|YyyRepository,ZzzRepository|データベーステーブルに対応するリポジトリインタフェースを格納する|
|7|src/main/java/root/domain/service/xxx|XxxService, XxxException|業務処理、業務例外クラスを格納する。業務ごとにサブディレクトリを作成する|
|8|src/main/java/root/util|AaaUtil,BbbUtil,CccUtil|共通処理クラスを格納する|
|9|src/main/resources|application.properites, messages.properties, shcema.sql, data.sql|設定ファイル、メッセージファイル、初期データスクリプトを格納する|
|10|src/main/resources/static/javascripts|Xxx.js|JavaScript ファイルを格納する。JavaScript ファイルは画面ごとに作成する|
|11|src/main/resources/static/stylesheets|style.css|スタイルシートを格納する|
|12|src/test/java/root/app/xxx|XxxControllerTest|画面単体テストクラスを格納する|
|13|src/test/java/root/domain/service|XxxServiceTest|業務処理テストクラスを格納する|
|14|target/classes|-|コンパイルしたクラスファイルを格納する|
|15|target/site/apidocs|-|生成した JavaDoc を格納する|
|16|target/test-classes|-|コンパイルしたテストクラスファイルを格納する|
  
  
---
それでは実際にプロジェクトを作成しましょう。

- [プロジェクトの作成](./02_create_project.md)
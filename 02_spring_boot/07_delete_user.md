ユーザ削除画面の作成
=====

この章では以下の内容を紹介します。

- 削除処理

---
## ブランチの作成

これまで同様、作業用ブランチを作成して移動します。

```sh
$ git branch
* master

$ git checkout -b 07_delete_user
Switched to a new branch '07_delete_user'

$ git branch
* 07_delete_user
  master
```


---
## ビューテンプレートの変更

まず、一覧画面から削除画面へ遷移するアンカーを追加します。`src/main/resources/templates/users/index.html` を以下のように変更してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
(snip)
	<table>
		<thead>
			<tr>
				<th>id</th>
				<th>名前</th>
				<th>メールアドレス</th>
				<th></th>
				<th></th> <!-- (7-1) -->
			</tr>
		</thead>
		<tbody>
			<tr th:each="user : ${users}">
				<td th:text="${user.id}">1</td>
				<td th:text="${user.name}">name</td>
				<td th:text="${user.email}">email@example.com</td>
				<td><a th:href="@{/users/update(id=${user.id})}">更新</a></td>
				<td><a th:href="@{/users/delete(id=${user.id})}">削除</a></td> <!-- (7-2) -->
			</tr>
		</tbody>
	</table>
(snip)
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|7-1| `<th></th>` | 空のヘッダ列を追加する |
|7-2| `<td><a th:href="@{/users/delete(id=${user.id})}">削除</a></td>` | リクエストパラメータ `id` に各ユーザの id を指定したパスを生成する |


## サービスクラスの変更

次に、サービスクラス `src/main/java/sample/domain/service/users/UsersService.java` にユーザ削除処理を追加します。

```java
package sample.domain.service.users;
(snip)
public class UsersService {
	(snip)
	public void delete(User user) {
		usersRepository.delete(user.getId());
	}
}
```

JpaRepository が提供する削除処理に委譲するだけで、特筆すべき内容はありません。


## ビューテンプレートの作成

続いて、ビューテンプレートを作成します。以下の内容で `src/main/resources/templates/users/delete.html` を作成してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
<head>
<meta charset="UTF-8" />
<title>削除</title>
</head>
<body>
	<h1>ユーザ削除</h1>
	<form th:action="@{/users/delete}" method="post" th:object="${userForm}">
		<div>
			<label for="name">名前:</label>
			<p th:text="*{name}"></p> <!-- (7-1) -->
		</div>
		<div>
			<label for="email">メールアドレス:</label>
			<p th:text="*{email}"></p>
		</div>
		<div>
			<input type="hidden" th:field="*{id}" />
			<button>削除</button>
		</div>
	</form>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。なお、更新処理と重複する内容は省略します。

|項番|内容|詳細|
|-|-|-|
|7-1| `<p th:text="*{name}"></p>` | `th:object` に指定した `UserForm` の `name` フィールドの値を表示する。資料の構成上、ここでは `p` タグを使用する。以下同様 |


## コントローラの変更

最後に、作成したビューテンプレート、および変更したサービスクラスを使って削除処理を行うよう `UserController` を変更します。

```java
package sample.app.users;
(snip)
public class UsersController {
(snip)
	@GetMapping("delete")
	public String delete(@RequestParam("id") Integer id, UserForm form) {
		User user = usersService.findById(id);
		BeanUtils.copyProperties(user, form);
		return "users/delete";
	}
	
	@PostMapping("delete")
	public String delete(UserForm form, RedirectAttributes redirect) {
		User user = toUser(form);
		usersService.delete(user);
		redirect.addFlashAttribute("success", "削除成功");
		return "redirect:/users/";
	}
(snip)
}
```

更新処理と重複する内容は省略します。特筆すべき内容はありません。


## 削除処理の確認

最後に、作成した削除処理の動作を確認します。

ブラウザから再度 http://localhost:8080/users/ にアクセスし、一覧画面に削除画面へのアンカーが表示されることを確認します。  
![](images/07_001.JPG)  
  

アンカーをクリックして削除画面に遷移し、当該ユーザの情報が表示されることを確認します。  
![](images/07_002.JPG)  
  

削除ボタンをクリックして削除処理が行えることを確認してください。  
![](images/07_003.JPG)  
![](images/07_004.JPG)  
  

併せて Hibernate が発行した SQL のログがコンソールに出力されていることも確認しておきましょう。
```
2017-04-02 08:02:54.739 DEBUG 47075 --- [nio-8080-exec-5] org.hibernate.SQL                        : 
    delete 
    from
        users 
    where
        id=?
2017-04-02 08:02:54.741 TRACE 47075 --- [nio-8080-exec-5] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [INTEGER] - [3]
```


---
## コミットとマージ

以上でユーザ削除画面の作成は完了です。ここまでの作業内容を git に保存しましょう。

`git status` コマンドでブランチの状態を確認します。
```sh
$ git status
# On branch 07_delete_user
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   src/main/java/sample/app/users/UsersController.java
#	modified:   src/main/java/sample/domain/service/users/UsersService.java
#	modified:   src/main/resources/templates/users/index.html
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/main/resources/templates/users/delete.html
no changes added to commit (use "git add" and/or "git commit -a")
```

`git diff` で差分に問題がないことを確認したら、カレントディレクトリの内容をすべてリポジトリに追加してコミットします。
```sh
$ git diff

$ git add .

$ git commit -m "削除画面を作成"
[07_delete_user 7ae8499] 削除画面を作成
 4 files changed, 46 insertions(+)
 create mode 100644 src/main/resources/templates/users/delete.html

$ git status
# On branch 07_delete_user
nothing to commit (working directory clean)
```

最後に、`master` ブランチにマージして作業用ブランチを削除します。
```sh
$ git checkout master
Switched to branch 'master'

$ git merge 07_delete_user 
Updating 9ccb539..7ae8499
Fast-forward
 src/main/java/sample/app/users/UsersController.java         |   15 +++++++++++++++
 src/main/java/sample/domain/service/users/UsersService.java |    4 ++++
 src/main/resources/templates/users/delete.html              |   25 +++++++++++++++++++++++++
 src/main/resources/templates/users/index.html               |    2 ++
 4 files changed, 46 insertions(+)
 create mode 100644 src/main/resources/templates/users/delete.html

$ git branch -d 07_delete_user 
Deleted branch 07_delete_user (was 7ae8499).
```

---
続いてユーザ検索処理を作成しましょう。

- [ユーザ検索](./08_search_users.md)
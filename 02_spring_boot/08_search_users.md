ユーザ検索処理
=====

この章では以下の内容を紹介します。

- 検索処理

---
## ブランチの作成

これまで同様、作業用ブランチを作成して移動します。

```sh
$ git branch
* master

$ git checkout -b 08_search_users
Switched to a new branch '08_search_users'

$ git branch
* 08_search_users
  master
```


---
## ビューテンプレートの変更

まず、一覧画面に検索フォームを追加します。`src/main/resources/templates/users/index.html` を以下のように変更してください。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
(snip)
	<h1>ユーザ一覧</h1>
	<div th:if="${success}">Success!</div>
	<form th:action="@{/users/search}" method="get" th:object="${userForm}">
		<div>
			<label for="name">名前:</label>
			<input type="text" th:field="*{name}"/>
		</div>
		<div>
			<label for="email">メールアドレス:</label>
			<input type="text" th:field="*{email}"/>
		</div>
		<div>
			<button>検索</button>
		</div>
	</form>
	<table>
(snip)
</html>
```

これまで同様、検索処理にも `UserForm` を使います。`get` メソッドを使う点を除いて、特筆すべき内容はありません。

なお、ここでは検索条件の仕様を以下のとおりとします。

- 検索条件が指定されていない場合は全件検索
- 名前、メールアドレスが入力されている場合は前方一致で検索
- 名前、メールアドレス両方が入力されている場合は AND 検索


## リポジトリの変更

次に、指定された検索条件に合致するユーザを検索する処理を追加します。

これまでの検索処理は検索条件が静的に決まっていたため Spring Data JPA のクエリ生成機能を使いましたが、検索条件の組み合わせが動的に変わる場合は `Specifications` という仕組みを使います。

本資料では `Specifications` の使い方のみを紹介します。詳細は下記リンクを参照してください。

- [Spring Data JPA - Reference Documentation](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#specifications)

`src/main/java/sample/domain/repository/UsersRepository.java` を以下のように変更します。

```java
package sample.domain.repository;
(snip)
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UsersRepository extends JpaRepository<User, Integer>,
	JpaSpecificationExecutor<User> { // (8-1)
(snip)
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|8-1| `JpaSpecificationExecutor<User>` | `User` を型パラメータとする `JpaSpecificationExecutor` を継承することで当該インタフェースで宣言されている各種メソッドを実装したプロキシクラスが生成される |


## `Specifications` の作成

検索仕様を表現する `Specifications` を作成します。以下の内容で `src/main/java/sample/domain/repository/UserSpecifications.java` を作成してください。

```java
package sample.domain.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import sample.domain.model.User;

public class UserSpecifications {

	public static Specification<User> nameContains(String name) {
		return StringUtils.isEmpty(name) ? null : (root, query, cb) -> { // (8-1), (8-2)
			return cb.like(root.get("name"), name + "%"); // (8-3)
		};
	}
	
	public static Specification<User> emailContains(String email) {
		return StringUtils.isEmpty(email) ? null : (root, query, cb) -> {
			return cb.like(root.get("email"), email + "%");
		};
	}
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|8-1| `StringUtils.isEmpty(name) ? null` | `name` が空の場合は null を返すことで検索条件から除外される |
|8-2| `(root, query, cb) -> ` | `name` が空でない場合は `Specifications<User>` を返す。当該インタフェースに宣言されているメソッドは `Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb)` ただひとつであるため、ラムダ式で記述できる | 
|8-3| `cb.like(root.get("name"), name + "%")` | 入力された検索条件の末尾に `%` を付加して LIKE 検索する |


## サービスの変更

次に、サービスクラス `src/main/java/sample/domain/service/users/UsersService.java` にユーザ検索処理を追加します。

```java
package sample.domain.service.users;

import static sample.domain.repository.UserSpecifications.*; // (8-1)
import org.springframework.data.jpa.domain.Specifications;
(snip)
public class UsersService {
(snip)
	@Transactional(readOnly = true)
	public List<User> findAll(User user) {
		return usersRepository.findAll(Specifications // (8-2)
				.where(nameContains(user.getName())) // (8-3)
				.and(emailContains(user.getEmail())));
	}
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|8-1| `import static sample.domain.repository.UserSpecifications.*;` | 作成した `Specification` を取得するメソッドを static インポートする |
|8-2| `Specifications` | Specifications クラスの static メソッドチェーンを使って動的に SQL を生成する |
|8-3| `.where()`, `.and()` | 引数に指定した Specification が null でない場合、適切に WHERE 句を生成する。以下同様 |


## コントローラの変更

最後に、変更したビューテンプレートとサービスクラスを使って検索処理を行うよう `UserController` を変更します。

```java
package sample.app.users;
(snip)
public class UsersController {
(snip)
	@GetMapping("search")
	public String search(UserForm form, Model model) {
		List<User> users = usersService.findAll(toUser(form));
		model.addAttribute("users", users);
		return "users/index";
	}
}
```

これまでの処理と同様です。特筆すべき内容はありません。


## 検索処理の確認

最後に、作成した検索処理の動作を確認します。

ブラウザから再度 http://localhost:8080/users/ にアクセスし、一覧画面に検索フォームが表示されることを確認します。  
![](images/08_001.JPG)  
  

名前の前方一致で検索できることを確認します。  
![](images/08_002.JPG)  
![](images/08_003.JPG)  
  

同様に、メールアドレスの前方一致で検索できることを確認します。  
![](images/08_004.JPG)  
![](images/08_005.JPG)  
  

名前とメールアドレスの AND 条件で検索できることも確認しましょう。  
![](images/08_006.JPG)  
![](images/08_007.JPG)  
  

併せて Hibernate が発行した SQL のログがコンソールに出力されていることも確認しておきましょう。
```
2017-04-02 08:21:36.907 DEBUG 48186 --- [nio-8080-exec-9] org.hibernate.SQL                        : 
    select
        user0_.id as id1_0_,
        user0_.email as email2_0_,
        user0_.name as name3_0_,
        user0_.password as password4_0_ 
    from
        users user0_ 
    where
        user0_.name like ?
2017-04-02 08:21:36.908 TRACE 48186 --- [nio-8080-exec-9] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [VARCHAR] - [ユーザ%]
2017-04-02 08:21:46.262 DEBUG 48186 --- [nio-8080-exec-1] org.hibernate.SQL                        : 
    select
        user0_.id as id1_0_,
        user0_.email as email2_0_,
        user0_.name as name3_0_,
        user0_.password as password4_0_ 
    from
        users user0_ 
    where
        user0_.email like ?
2017-04-02 08:21:46.262 TRACE 48186 --- [nio-8080-exec-1] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [VARCHAR] - [user1%]
2017-04-02 08:21:54.666 DEBUG 48186 --- [nio-8080-exec-2] org.hibernate.SQL                        : 
    select
        user0_.id as id1_0_,
        user0_.email as email2_0_,
        user0_.name as name3_0_,
        user0_.password as password4_0_ 
    from
        users user0_ 
    where
        (
            user0_.name like ?
        ) 
        and (
            user0_.email like ?
        )
2017-04-02 08:21:54.666 TRACE 48186 --- [nio-8080-exec-2] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [VARCHAR] - [ユーザ%]
2017-04-02 08:21:54.667 TRACE 48186 --- [nio-8080-exec-2] o.h.type.descriptor.sql.BasicBinder      : binding parameter [2] as [VARCHAR] - [user1%]
```

---
## コミットとマージ

以上でユーザ検索機能の作成は完了です。ここまでの作業内容を git に保存しましょう。

`git status` コマンドでブランチの状態を確認します。
```sh
$ git status
# On branch 08_search_users
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   src/main/java/sample/app/users/UsersController.java
#	modified:   src/main/java/sample/domain/repository/UsersRepository.java
#	modified:   src/main/java/sample/domain/service/users/UsersService.java
#	modified:   src/main/resources/templates/users/index.html
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/main/java/sample/domain/repository/UserSpecifications.java
no changes added to commit (use "git add" and/or "git commit -a")
```

`git diff` で差分に問題がないことを確認したら、カレントディレクトリの内容をすべてリポジトリに追加してコミットします。
```sh
$ git diff

$ git add .

$ git commit -m "検索処理を作成"
[08_search_users ead3ef5] 検索処理を作成
 5 files changed, 54 insertions(+), 1 deletion(-)
 create mode 100644 src/main/java/sample/domain/repository/UserSpecifications.java

$ git status
# On branch 08_search_users
nothing to commit (working directory clean)
```

最後に、`master` ブランチにマージして作業用ブランチを削除します。
```sh
$ git checkout master
Switched to branch 'master'

$ git merge 08_search_users 
Updating 7ae8499..ead3ef5
Fast-forward
 src/main/java/sample/app/users/UsersController.java            |    7 +++++++
 src/main/java/sample/domain/repository/UserSpecifications.java |   21 +++++++++++++++++++++
 src/main/java/sample/domain/repository/UsersRepository.java    |    4 +++-
 src/main/java/sample/domain/service/users/UsersService.java    |   10 ++++++++++
 src/main/resources/templates/users/index.html                  |   13 +++++++++++++
 5 files changed, 54 insertions(+), 1 deletion(-)
 create mode 100644 src/main/java/sample/domain/repository/UserSpecifications.java

$ git branch -d 08_search_users 
Deleted branch 08_search_users (was ead3ef5).
```

---
続いて共通レイアウトを作成しましょう。

- [共通レイアウト](./09_layout.md)
ユーザ登録画面の作成
=====

この章では以下の内容を紹介します。

- フォームの作成
- バリデータの作成
- 業務例外の作成
- 登録処理

---
## ブランチの作成

これまで同様、作業用ブランチを作成して移動します。

```sh
$ git branch
* master

$ git checkout -b 05_create_user
Switched to a new branch '05_create_user'

$ git branch
* 05_create_user
  master
```

---
## サービスクラスの変更

まず、サービスクラス `src/main/java/sample/domain/service/users/UsersService.java` にユーザ登録処理を追加します。

```java
package sample.domain.service.users;
(snip)
public class UsersService {
	(snip)
	public User create(User user) {
		return usersRepository.save(user);
	}
}
```

JpaRepository が提供する `save` に処理を委譲するだけで、特筆すべき内容はありません。


## フォームの作成

次に、ユーザ登録画面の入力内容を受け取るフォームクラスを `src/main/java/sample/app/users/UserForm` に作成します。

```java
package sample.app.users;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import lombok.Data;

@Data
public class UserForm {
	
	@NotNull // (5-1)
	@Size(max = 60) // (5-2)
	private String name;

	@NotNull
	@Size(max = 60)
	@Email // (5-3)
	private String email;

	@NotNull
	@Size(min = 8, max = 60)
	private String password;

	@NotNull
	@Size(min = 8, max = 60)
	private String password2;
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-1| `@NotNull` | 当該フィールドは Null でないことを示す |
|5-2| `@Size(max = 60)` | 当該フィールドは 60 文字までの範囲で入力することを示す。`min` を省略した場合は `0` が設定される |
|5-3| `@Email` | 当該フィールドはメールアドレス形式で入力することを示す |


## ビューテンプレートの作成

続いて、作成したフォームを使ってユーザ登録処理を行うビューテンプレートを `src/main/resources/templates/users/create.html` に作成します。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
<head>
<meta charset="UTF-8" />
<title>登録</title>
</head>
<body>
	<h1>ユーザ登録</h1>
	<form th:action="@{/users/create}" method="post" th:object="${userForm}"> <!-- (5-1), (5-2) -->
		<div th:if="${#fields.hasAnyErrors()}">Error!</div> <!-- (5-3) -->
		<div>
			<label for="name">名前:</label>
			<input type="text" th:field="*{name}"/> <!-- (5-4) -->
		</div>
		<div>
			<label for="email">メールアドレス:</label>
			<input type="text" th:field="*{email}"/>
		</div>
		<div>
			<label for="password">パスワード:</label>
			<input type="password" th:field="*{password}"/>
		</div>
		<div>
			<label for="password2">パスワード (再入力):</label>
			<input type="password" th:field="*{password2}"/>
		</div>
		<div>
			<button>登録</button>
		</div>
	</form>
</body>
</html>
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-1| `th:action="@{/users/create}"` | コンテキストパスを含めたアクションパスを出力する |
|5-2| `th:object="${userForm}"` | 入力フォームで取り扱うオブジェクトを示す。ここでは上記で作成した UserForm を使う |
|5-3| `th:if="${#fields.hasAnyErrors()}"` | 入力エラーが発生した場合に当該タグの内容を表示する |
|5-4| `th:field="*{name}"` | `th:object` で宣言したオブジェクトのフィールドを示す。この場合は当該フィールドと UserForm クラスの name フィールドが対応付けられる。以下同様 |


## コントローラの変更

最後に、作成したフォームクラスとビューテンプレート、および変更したサービスクラスを使って登録処理を行うよう `UserController` を変更します。

```java
package sample.app.users;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
(snip)
public class UsersController {
(snip)
	@ModelAttribute("userForm") // (5-1)
	public UserForm setUpUserForm() {
		return new UserForm();
	}
	
	@InitBinder("userForm") // (5-2)
	public void bindUserForm(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true)); // (5-3)
	}
(snip)
	@GetMapping("create") // (5-4)
	public String create() {
		return "users/create";
	}
	
	@PostMapping("create") // (5-5)
	public String create(@Validated UserForm form, // (5-6)
			BindingResult result, // (5-7)
			RedirectAttributes redirect) { // (5-8)

		if (result.hasErrors()) { // (5-9)
			return "users/create";
		}
		
		usersService.create(toUser(form)); // (5-10)
		
		redirect.addFlashAttribute("success", "登録成功"); // (5-11)
		
		return "redirect:/users/"; // (5-12)
	}

	private User toUser(UserForm form) {
		User user = new User();
		BeanUtils.copyProperties(form, user); // (5-13)
		return user;
	}
}
```

記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-1| `@ModelAttribute("userForm")` | model に設定するオブジェクトを作成する。ここでは `userForm` という名前で `UserForm` オブジェクトを設定する |
|5-2| `@InitBinder("userForm")` | フォームの入力内容を `UserForm` オブジェクトにバインドする際の処理をカスタマイズする |
|5-3| `binder.registerCustomEditor(String.class, new StringTrimmerEditor(true))` | バインド処理の具体的なカスタマイズ内容を記述する。ここでは、フォームになにも入力されなかった場合に設定される空文字を Null に置き換える `StringTrimmerEditor` を設定する |
|5-4| `@GetMapping("create")` | `create` に対する GET リクエストを処理することを示す |
|5-5| `@PostMapping("create")` | `create` に対する POST リクエストを処理することを示す |
|5-6| `@Validated UserForm` | バインダーによってフォームの入力内容が設定された `UserForm` を受け取る。`@Validated` を指定すると妥当性検査が実行される |
|5-7| `BindingResult` | 妥当性検査結果を受け取る。`@Validated` を指定した引数の直後に指定する必要があることに注意 |
|5-8| `RedirectAttributes` | POST 処理成功後は PRG パターンを使用するため、リダイレクト処理間で値を保持する `RedirectAttributes` を用意する |
|5-9| `result.hasErrors()` | 妥当性検査結果を確認する。入力エラーが発生している場合は入力画面に戻る |
|5-10| `usersService.create(toUser(form))` | フォームクラスをモデルクラスに変更して保存する |
|5-11| `redirect.addFlashAttribute("success", "登録成功")` | 登録処理が成功した場合は Flash スコープに処理成功メッセージを格納する |
|5-12| `return "redirect:/users/"` | URL `/users/` にリダイレクトする。ビューレンプレートパスではないことに注意 |
|5-13| `BeanUtils.copyProperties` | 型と名前が一致するプロパティをコピーする |


## 登録処理の確認

登録処理の動作を確認します。

ユーザ一覧画面 `src/main/resources/templates/users/index.html` にユーザ登録画面へ遷移するアンカーを追加しましょう。また、登録処理成功時にメッセージを表示します。

```html
<!DOCTYPE html>
<html
	xmlns:th="http://www.thymeleaf.org/">
(snip)
	<h1>ユーザ一覧</h1>
	<div th:if="${success}">Success!</div> <!-- (5-1) -->
(snip)
	</table>
	<a th:href="@{/users/create}">新規作成</a> <!-- (5-2) -->
</body>
</html>
```

記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-1| `<div th:if="${success}">Success!</div>` | `success` が存在する場合に当該タグを出力する |
|5-2| `th:href="@{/users/create}` | `th:action` と同様にコンテキストパスを含むリンクを生成する |

ブラウザから http://localhost:8080/users にアクセスし、ユーザ登録画面に遷移するアンカーが表示されることを確認しましょう。  
![](images/05_001.JPG)  
  

当該アンカーをクリックし、作成したビューテンプレートの内容が表示されることを確認します。  
![](images/05_002.JPG)  
  

なにも入力せずに登録ボタンをクリックすると期待通り登録処理に失敗します。  
![](images/05_003.JPG)  
![](images/05_004.JPG)  
  

不正な文字数やメールアドレスを入力すると、やはり登録処理に失敗します。  
![](images/05_005.JPG)  
![](images/05_006.JPG)  
  

すべての項目を正しく入力すると登録処理に成功します。  
![](images/05_007.JPG)  
![](images/05_008.JPG)  
  

以上で基本的な登録処理の実装は完了です。

ただし、これまでの実装ではパスワード欄とパスワード(再入力)欄の内容が一致していなくても登録処理に成功してしまいます。以下、これを適切に制御するバリデータの作成方法を紹介します。


## バリデータの作成

ここでは Spring Validator を使って相関チェックを行うバリデータを作成します。以下の内容で `src/main/java/sample/app/users/UserFormValidator.java` を作成してください。

```java
package sample.app.users;

import java.util.Objects;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component // (5-1)
public class UserFormValidator implements Validator { // (5-2)

	@Override
	public boolean supports(Class<?> clazz) {
		return UserForm.class.isAssignableFrom(clazz); // (5-3)
	}

	@Override
	public void validate(Object target, Errors errors) {
		UserForm form = (UserForm) target;
		if (!Objects.equals(form.getPassword(), form.getPassword2())) { // (5-4)
			errors.rejectValue("password", "error"); // (5-5)
		}
	}
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-1| `@Component` | コンポーネントクラスであることを示す。Spring コンテナに登録される |
|5-2| `implements Validator` | Spring Validator を実装する |
|5-3| `UserForm.class.isAssignableFrom(clazz)` | UserForm に対応したバリデータであることを示す |
|5-4| `Objects.equals` | パスワードとパスワード(再入力) の内容を比較する |
|5-5| `errors.rejectValue("password", "error")` | 両者が一致しない場合は前者の入力エラーとする |


## コントローラの変更

コントローラに作成したバリデータを設定します。`src/main/java/sample/app/users/UsersController.java` を以下のとおり変更してください。

```java
package sample.app.users;
(snip)
@Controller
@RequestMapping("users")
public class UsersController {
(snip)
	private final UserFormValidator userFormValidator; // (5-14)
(snip)
	@InitBinder("userForm")
	public void bindUserForm(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.addValidators(userFormValidator); // (5-15)
	}
(snip)
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-14| `private final UserFormValidator userFormValidator` | コンストラクタインジェクションするフィールド |
|5-15| `binder.addValidators(userFormValidator)` | インジェクションしたバリデータをバインダーに追加する |

ブラウザから 再度 http://localhost:8080/users/crete にアクセスし、パスワード欄とパスワード(再入力)欄の内容が一致していない場合は入力エラーになることを確認します。  
![](images/05_009.JPG)  
![](images/05_010.JPG)  
  

両者が一致している場合は登録処理に成功することも確認しましょう。  
![](images/05_011.JPG)  
![](images/05_012.JPG)  
  

以上でバリデータの作成は完了です。

最後に、すでに存在するメールアドレスを指定した場合は業務例外とする処理を追加しましょう。


## 業務例外クラスの作成

まず、以下の内容で業務例外クラスを `src/main/java/sample/domain/service/users/EmailAlreadyExistException.java` に作成してください。

```java
package sample.domain.service.users;

public class EmailAlreadyExistException extends RuntimeException {
	private static final long serialVersionUID = 1L;
}
```

実行時例外を継承するだけで、特筆すべき内容はありません。


## リポジトリの変更

次に、リポジトリインタフェース `src/main/java/sample/domain/repository/UsersRepository.java` にメールアドレスをキーにデータを一件取得するメソッドを追加します。

```java
package sample.domain.repository;
(snip)
import sample.domain.model.User;

public interface UsersRepository extends JpaRepository<User, Integer> {
	User findOneByEmail(String email); // (5-1)
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-1| `findOneByEmail(String email)` | `email` の値が一致する `User` を一件取得するメソッドを自動生成する |

Spring Data JPA のクエリ生成命名規約の詳細については、以下のリンクを参照してください。

- [Spring Data JPA - Reference Documentation](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)


## サービスの変更

続いて、上記クエリを使ってすでに存在するメールアドレスが指定された場合は、同じく上記で作成した業務例外をスローするようサービスクラスを変更します。 `src/main/java/sample/domain/service/users/UsersService.java` に以下の処理を追加してください。

```java
package sample.domain.service.users;
(sip)
public class UsersService {
(snip)
	public User create(User user) {
		if (usersRepository.findOneByEmail(user.getEmail()) != null) {
			throw new EmailAlreadyExistException();
		}
		return usersRepository.save(user);
	}
}
```

特筆すべき内容はありません。


## コントローラの変更

最後に、スローされた例外をキャッチしてハンドリングするようコントローラを変更します。`src/main/java/sample/app/users/UsersController.java` に以下の処理を追加してください。

```java
package sample.app.users;

import sample.domain.service.users.EmailAlreadyExistException;
import org.springframework.validation.ObjectError;
(snip)
public class UsersController {
(snip)
	@PostMapping("create")
	public String create(@Validated(Create.class) UserForm form,
			BindingResult result,
			RedirectAttributes redirect) {
(snip)
		try {
			usersService.create(toUser(form)); // (5-10)
		} catch (EmailAlreadyExistException e) {
			result.addError(new ObjectError("email", e.getMessage())); // (5-16)
			return "users/create";
		}
(snip)
	}
(snip)
}
```

各記述内容の詳細は以下のとおりです。

|項番|内容|詳細|
|-|-|-|
|5-16| `result.addError(new ObjectError("email", e.getMessage()));` | `email` に関する入力エラーを追加する |


ブラウザから 再度 http://localhost:8080/users/crete にアクセスし、すでに存在するメールアドレスを指定した場合は入力エラーになることを確認します。  
![](images/05_013.JPG)  
![](images/05_014.JPG)  
  

まだ存在しないメールアドレスを指定した場合は登録処理に成功することも確認しましょう。  
![](images/05_015.JPG)  
![](images/05_016.JPG)  
  

併せて Hibernate が発行した SQL のログがコンソールに出力されていることも確認しておきましょう。
```sh
2017-04-02 06:48:43.824 DEBUG 97821 --- [nio-8080-exec-9] org.hibernate.SQL                        : 
    select
        user0_.id as id1_0_,
        user0_.email as email2_0_,
        user0_.name as name3_0_,
        user0_.password as password4_0_ 
    from
        users user0_ 
    where
        user0_.email=?
2017-04-02 06:48:43.840 TRACE 97821 --- [nio-8080-exec-9] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [VARCHAR] - [user4@example.com]
2017-04-02 06:48:43.951 DEBUG 97821 --- [nio-8080-exec-9] org.hibernate.SQL                        : 
    insert 
    into
        users
        (id, email, name, password) 
    values
        (null, ?, ?, ?)
2017-04-02 06:48:43.952 TRACE 97821 --- [nio-8080-exec-9] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [VARCHAR] - [user4@example.com]
2017-04-02 06:48:43.953 TRACE 97821 --- [nio-8080-exec-9] o.h.type.descriptor.sql.BasicBinder      : binding parameter [2] as [VARCHAR] - [ユーザ4]
2017-04-02 06:48:43.953 TRACE 97821 --- [nio-8080-exec-9] o.h.type.descriptor.sql.BasicBinder      : binding parameter [3] as [VARCHAR] - [password]
```

---
## コミットとマージ

以上でユーザ登録画面の作成は完了です。ここまでの作業内容を git に保存しましょう。

`git status` コマンドでブランチの状態を確認します。
```
$ git status
# On branch 05_create_user
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   src/main/java/sample/app/users/UsersController.java
#	modified:   src/main/java/sample/domain/repository/UsersRepository.java
#	modified:   src/main/java/sample/domain/service/users/UsersService.java
#	modified:   src/main/resources/templates/users/index.html
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/main/java/sample/app/users/UserForm.java
#	src/main/java/sample/app/users/UserFormValidator.java
#	src/main/java/sample/domain/service/users/EmailAlreadyExistException.java
#	src/main/resources/templates/users/create.html
no changes added to commit (use "git add" and/or "git commit -a")
```

`git diff` で差分を確認します。差分内容が一画面に収まらない場合は `space` で次ページ、`b` で前ページへ遷移して確認します。
```sh
$ git diff
```

差分に問題がないことを確認したら、`q` で差分画面を閉じて、カレントディレクトリの内容をすべてリポジトリに追加してコミットします。
```sh
$ git add .

$ git commit -m "登録画面を作成"
[05_create_user 1d91a3e] 登録画面を作成
 8 files changed, 154 insertions(+)
 create mode 100644 src/main/java/sample/app/users/UserForm.java
 create mode 100644 src/main/java/sample/app/users/UserFormValidator.java
 create mode 100644 src/main/java/sample/domain/service/users/EmailAlreadyExistException.java
 create mode 100644 src/main/resources/templates/users/create.html

$ git status
# On branch 05_create_user
nothing to commit (working directory clean)
```

最後に、`master` ブランチにマージして作業用ブランチを削除します。
```sh
$ git checkout master
Switched to branch 'master'

$ git merge 05_create_user 
Updating aa5bc7f..1d91a3e
Fast-forward
 src/main/java/sample/app/users/UserForm.java                              |   27 +++++++++++++++++++++++++++
 src/main/java/sample/app/users/UserFormValidator.java                     |   24 ++++++++++++++++++++++++
 src/main/java/sample/app/users/UsersController.java                       |   55 +++++++++++++++++++++++++++++++++++++++++++++++++++++++
 src/main/java/sample/domain/repository/UsersRepository.java               |    1 +
 src/main/java/sample/domain/service/users/EmailAlreadyExistException.java |    5 +++++
 src/main/java/sample/domain/service/users/UsersService.java               |    7 +++++++
 src/main/resources/templates/users/create.html                            |   33 +++++++++++++++++++++++++++++++++
 src/main/resources/templates/users/index.html                             |    2 ++
 8 files changed, 154 insertions(+)
 create mode 100644 src/main/java/sample/app/users/UserForm.java
 create mode 100644 src/main/java/sample/app/users/UserFormValidator.java
 create mode 100644 src/main/java/sample/domain/service/users/EmailAlreadyExistException.java
 create mode 100644 src/main/resources/templates/users/create.html

$ git branch -d 05_create_user 
Deleted branch 05_create_user (was 1d91a3e).
```


---
続いてユーザ編集画面を作成しましょう。

- [ユーザ編集](./06_update_user.md)

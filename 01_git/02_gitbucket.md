GitBucket
=====

本資料では GitBucket の基本的な使い方を紹介します。


## ログイン

Web ブラウザから GitBucket にアクセスし、`Sign in` をクリックします。  
![](./images/02_001.JPG)  

`root`/`root` でログインします。  
![](./images/02_002.JPG)  

以上でログインは完了です。


## ユーザの作成

Administrator 権限を持つユーザでログインし、プルダウンメニューから `System administration` を選択します。  
![](./images/02_003.JPG)  

`New user` ボタンをクリックします。  
![](./images/02_004.JPG)  

必要な項目を入力して `Create user` ボタンをクリックします。  
![](./images/02_005.JPG)  

ユーザが作成されたことを確認します。  
![](./images/02_005a.JPG)  

以上でユーザの作成は完了です。


## リポジトリの作成

プルダウンメニューから `New repository` を選択します。  
![](./images/02_006.JPG)  

必要な項目を入力して `Create repository` ボタンをクリックします。  
![](./images/02_007.JPG)  

リポジトリが作成されたら画面左のメニューから `Settings` を選択します。  
![](./images/02_008.JPG)  

`Collaborators` タブをクリックします。  
![](./images/02_009.JPG)  

リポジトリに招待するユーザを `Collaborators` に入力して `Add` ボタンをクリックします。  
![](./images/02_010.JPG)  

招待したユーザの権限を選択したら `Apply changes` ボタンをクリックします。  
![](./images/02_011.JPG)  

以上でリポジトリの作成は完了です。


## 課題の作成

リポジトリのトップ画面にアクセスし、画面左のメニューから `Issues` を選択します。  
![](./images/02_012.JPG)  

`New issue` ボタンをクリックします。  
![](./images/02_013.JPG)  

タイトルと内容を入力します。内容は markdown で記述できます。  
![](./images/02_014.JPG)  

markdown のレンダリング結果は `Preview` タブで確認します。  
![](./images/02_015.JPG)  

必要に応じて `Labels` や `Milestone`、`Assignee` を設定します。  
![](./images/02_016.JPG)  

入力が完了したら `Submit new issue` をクリックします。  
![](./images/02_017.JPG)  

課題が作成されたことを確認します。  
![](./images/02_018.JPG)  

以上で課題の作成は完了です。

Git を使った開発フロー
=====

## はじめに

本資料では Git を使った開発フローを紹介します。

Git は Subversion と比べてブランチの作成や切り替えが軽快で、かつ正確にマージできるという特徴があるため、Git を使った開発フローではブランチの作成とマージを頻繁に行います。

このブランチの作成およびマージに関するベストプラクティスとして、`git-flow` と呼ばれる開発フローと、これを補助するツールがあります。詳細は下記リンクを参照してください。

- [見えないチカラ: A successful Git branching model を翻訳しました](http://keijinsonyaban.blogspot.jp/2010/10/a-successful-git-branching-model.html)
- [git-flow cheatsheet](http://danielkummer.github.io/git-flow-cheatsheet/index.ja_JP.html)


また、GitHub に代表される Git リポジトリホスティングサービスには Git の特徴を活かしたプルリクエストという機能があり、簡略化した `git-flow` とプルリクエストを組み合わせた `GitHub Flow` という開発フローがあります。詳細は下記リンクを参照してください。

- [GitHub Flow \(Japanese translation\) · GitHub](https://gist.github.com/Gab-km/3705015)
- [GitHub flowを用いた開発フロー \- Qiita](http://qiita.com/ryotakodaira/items/e860396ae44942dcca5e)


本資料では Git Bash および GitBucket を使って GitHub Flow を実施する手順を紹介します。



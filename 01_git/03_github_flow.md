開発フロー
=====

まず、リモートリポジトリを `origin` という名称で登録します。

ブラウザからリポジトリにアクセスして `copy to clipboard` ボタンをクリックしてリポジトリの URL をコピーしてください。
![](images/03_001.JPG)

リモートリポジトリからファイルを取得する場合は、このリモートリポジトリをクローンします。

すでに手元にあるローカルリポジトリのファイルをリモートリポジトリに反映する場合は、このリモートリポジトリを `origin` という名称で登録します。


## リモートリポジトリのクローン

Git Bash を開いて、必要に応じて作業用ディレクトリを作成して移動します。
```sh
$ mkdir -p /c/work/gitbucket/

$ cd /c/work/gitbucket/
```

コピーした URL を指定して `git clone` を実行します。
```sh
$ git clone http://localhost:9999/git/root/sample.git
Cloning into 'sample'...
remote: Counting objects: 3, done
remote: Finding sources: 100% (3/3)
remote: Getting sizes: 100% (2/2)
remote: Compressing objects: 100% (60/60)
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
```

clone したディレクトリに移動したら、`git branch -a` を実行してリモートリポジトリが `origin` という名前で登録されていることを確認します。
```sh
$ cd sample

$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
```

以上でリモートリポジトリのクローンは完了です。


## リモートリポジトリの追加

Git Bash を開いて、すでにローカルに存在するリポジトリへ移動したら、リモートリポジトリが登録されていないことを確認します。
```sh
$ cd sample

$ git branch -a
* master
```

リモートリポジトリの名前と URL を指定して `git remote add` コマンドを実行します。
```sh
$ git remote add origin http://localhost:9999/git/root/sample.git
```

`git fetch` コマンドでリモートリポジトリの状態を取得します。
```sh
$ git fetch
warning: no common commits
remote: Counting objects: 3, done
remote: Finding sources: 100% (3/3)
remote: Getting sizes: 100% (2/2)
remote: Compressing objects: 100% (60/60)
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From http://localhost:9999/git/root/sample
 * [new branch]      master     -> origin/master
```

`git pull` コマンドでリモートリポジトリのファイルを取得します。
```sh
$ git pull origin master
From http://localhost:9999/git/root/sample
 * branch            master     -> FETCH_HEAD
```

`git branch -a` を実行してリモートリポジトリが `origin` という名前で登録されていることを確認します。
```sh
$ git branch -a
* master
  remotes/origin/master
```

以上でリモートリポジトリの追加は完了です。


## 課題の登録

開発はすべて GitBucket に issue を登録することから始めます。期待する効果は以下のとおりです。

- 課題内容を明確に共有する
- 担当者を明確にする
- 将来の問題発生時に対応内容/時期/担当者を遡及して追跡する  

まず、画面左のメニューから `issues` を選択します。  
![](./images/02_012.JPG)  

`New Issue` ボタンをクリックします。  
![](./images/02_013.JPG)  

課題のタイトルなど、必要な項目を入力します。
![](./images/02_014.JPG)  

必要な項目を入力したら `Submit new issue` ボタンをクリックします。  
![](./images/02_017.JPG)  

課題が登録されたことを確認します。  
![](./images/02_018.JPG)  

以上で課題の登録は完了です。


## ブランチの作成

issue 対応は専用のブランチで作業します。まず `git branch` コマンドでブランチの状態を確認します。
```sh
$ git branch
* master
```

次に、`git branch` コマンドにブランチ名を指定して、ブランチを作成します。ブランチ名は `issue/(issue 番号)_(issue の内容)` とします。
```sh
$ git branch issue/1_create_top_page
```

先ほどと同様にブランチの状態を確認すると、新しいブランチが作成されていること、および現在は master ブランチ上にいることが分かります。
```sh
$ git branch
  issue/1_create_top_page
* master
```

最後に、`git checkout` コマンドで作成したブランチに移動します。
```sh
$ git checkout issue/1_create_top_page
Switched to branch 'issue/1_create_top_page'

$ git branch
* issue/1_create_top_page
  master
```

以上でブランチの作成は完了です。


## コミットとプッシュ

issue 対応用ブランチで作業した内容を、適当なタイミングで保存し、チームに公開します。Git では作業内容を保存することを `commit` と呼び、チームに公開することを `push` と呼びます。

GitHub Flow では、作業が完了していない状態でも積極的に push します。専用のブランチで作業しているため、push しても master ブランチには影響しません。期待する効果は以下のとおりです。
- 課題対応状況を早い段階で共有する
- 端末故障など不測の事態に備える

commit と push は開発者それぞれ任意のタイミングで実施して構いません。目安は以下のとおりです。
- commit
  - 作業内容がその後に手戻りすることはないと考えられる場合
  - 全体の枠組みを作り終えた場合
  - モジュール、単体テストをひとつ書き終えた場合
  - etc.
- push
  - 作業に着手したことを知らせたい場合
  - 実装内容に不安があり、周囲の意見を聞きたい場合
  - 共通処理など周囲と共有する成果物を書き終えた場合

後述のとおり、対応を完了した場合も当然 commit して push します。以下、commit と push の具体的な手順を紹介します。

---

まず、`git status` コマンドでローカルリポジトリの状態を確認します。
```sh
$ git status
On branch issue/1_create_top_page
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        src/main/java/sample/app/
        src/main/resources/templates/

nothing added to commit but untracked files present (use "git add" to track)
```

更新されたファイル、追加されたファイルがある場合は `git add .` コマンドを実行してリポジトリに追加します。
```sh
$ git add .

$ git status
On branch issue/1_create_top_page
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   src/main/java/sample/app/IndexController.java
        new file:   src/main/resources/templates/index.html
```

作業中のファイルがリポジトリに追加されたら、メッセージを指定してリポジトリの内容をコミットします。

この際、コミットコメントにコミット内容と `#nn` (**nn は課題番号に読み替える**) のように課題番号を入力することで、後述するとおり GitBucket 上で当該 issue とコミットが関連付けられるため、対応内容を容易に確認できます。
```sh
$ git commit -m "トップ画面を作成 #1"
[issue/1_create_top_page 7c6c87e] トップ画面を作成 #1
 2 files changed, 31 insertions(+)
 create mode 100644 src/main/java/sample/app/IndexController.java
 create mode 100644 src/main/resources/templates/index.html
```

コミットが完了したら、リモートリポジトリの名前とローカルリポジトリのブランチ名を指定して `git push` コマンドを実行します。
```sh
$ git push origin issue/1_create_top_page
Counting objects: 11, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (8/8), done.
Writing objects: 100% (11/11), 1.20 KiB | 0 bytes/s, done.
Total 11 (delta 1), reused 0 (delta 0)
remote: Resolving deltas: 100% (1/1)
remote: Updating references: 100% (1/1)
To http://localhost:9999/git/root/sample.git
 * [new branch]      issue/1_create_top_page -> issue/1_create_top_page
```

以上で commit と push は完了です。


## 対応状況の確認

プッシュされたブランチは GitBucket の `Branches` メニューから確認することができます。  
![](./images/03_002.JPG)  
![](./images/03_003.JPG)  

前述のとおり、コミットコメントに当該課題番号が入力されているコミットが自動的に関連付けられます。  
![](./images/03_004.JPG)  

当該コミットのハッシュ ID をクリックすることで、差分を確認することができます。  
![](./images/03_005.JPG)  
![](./images/03_006.JPG)  

このように対応状況を早い段階で共有することで、対応スケジュールの終盤で重大な認識齟齬に気付くなどという事態を未然に防止することができます。


## 課題対応の完了

引き続き適切なタイミングでコミットとプッシュをくり返し、課題の対応を完了します。  


## プルリクエスト

GitBucket のプルリクエスト機能を利用して、課題対応ブランチの内容を `master` ブランチにマージします。プルリクエストすることで以下の効果が期待できます。
- 担当者以外の要員にマージ (およびレビュー) を依頼することで、対応内容の属人化を防止する
- `master` ブランチを常にリリース可能な状態に保つ

以下、具体的なプルリクエスト、レビューおよびマージの手順を紹介します。

---

`Branches` メニューを開き、`master` ブランチにマージしたいブランチの `New Pull Request` ボタンをクリックします。  
![](./images/03_007.JPG)  

マージ可能な状態であることを示す `Able to merge.` が表示されることを確認したら、依頼内容を記入します。コンフリクトが発生した場合は、後述の手順に従ってコンフリクトを解消してください。
![](./images/03_008.JPG)  

この際、`@` に続けてユーザIDを指定することで特定の要員にレビューを依頼することができます。また、`Asignee` から特定の要員を指定することもできます。  
![](./images/03_009.JPG)  

画面下部に当該ブランチで実施されたコミット、および差分が表示されるので、それぞれ問題ないことを確認したら `Create pull request` ボタンをクリックします。
![](./images/03_010.JPG)  
![](./images/03_011.JPG)  

以上でプルリクエストは完了です。


## レビュー

レビューを依頼された要員は、`Pull Requests` メニューから当該プルリクエストを開きます。  
![](./images/03_012.JPG)  
![](./images/03_013.JPG)  

`Files Changed` タブから対応内容の差分を確認し、再対応が必要な箇所にコメントを記入します。  
![](./images/03_014.JPG)  

なお、レビューコメントを記入する際は以下の事項に留意してください。
- レビューアとレビューイは対等の立場であり、上下関係はない
- レビューコメントは欠陥の指摘ではなく、改善の提案
- 瑣末な誤字脱字やコーディング規約違反などに必要以上にこだわらない
- 機能性、運用性、保守性を最重要視する
- カジュアルな雰囲気を醸成するために、日常的な言葉遣いや顔文字、絵文字を活用する
  - `:xxx:` のようにコロンの間に特定の文字列を指定することで絵文字を入力できる
  - 絵文字一覧は [Emoji cheat sheet for GitHub, Basecamp and other services](http://www.webpagefx.com/tools/emoji-cheat-sheet/) を参照

![](./images/03_015.JPG)  

レビューが完了したら `Comment` ボタンをクリックします。入力したレビューコメントは当該プルリクエストのトップ画面にも表示されます。  
![](./images/03_016.JPG)  
![](./images/03_017.JPG)  
![](./images/03_018.JPG)  

以上でレビューは完了です。


## レビュー指摘事項の反映

担当者はレビューコメントを確認し、納得した場合は指摘事項を反映します。納得できない場合や別の対応案が考えられる場合は、レビューコメント欄に返信して議論します。

ここでもレビューの際と同様、レビューアとレビューイは対等の立場であること、双方の優劣ではなく課題の解決に集中することなどに留意し、建設的に議論しましょう。  
![](./images/03_019.JPG)  
![](./images/03_020.JPG)  
![](./images/03_021.JPG)  
![](./images/03_022.JPG)  

レビュー指摘事項の反映が完了したら、これまでと同じ手順で `add`, `commit`, `push` します。  

```sh
$ git status
On branch issue/1_create_top_page
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   src/main/java/sample/app/IndexController.java
        modified:   src/main/resources/templates/index.html

$ git add .

$ git commit -m "不要なコメントを削除 #1"
[issue/1_create_top_page ede0036] 不要なコメントを削除 #1
 2 files changed, 8 insertions(+), 8 deletions(-)

$ git push origin issue/1_create_top_page
Counting objects: 11, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (8/8), done.
Writing objects: 100% (11/11), 915 bytes | 0 bytes/s, done.
Total 11 (delta 3), reused 0 (delta 0)
remote: Resolving deltas: 100% (3/3)
remote: Updating references: 100% (1/1)
To http://localhost:9999/git/root/sample.git
   7c6c87e..ede0036  issue/1_create_top_page -> issue/1_create_top_page
```

すでにプルリクエストされたブランチに `push` すると、その内容は当該プルリクエストの自動的に反映されるので、改めてプルリクエストする必要はありません。  
![](./images/03_023.JPG)  
![](./images/03_024.JPG)  

以上でレビュー指摘事項の反映は完了です。


## マージ

レビューを依頼された要員は、レビュー指摘事項の内容を確認し、問題ない場合は `Merge pull request` ボタンをクリックします。  
![](./images/03_025.JPG)  

コメント欄に担当者への感謝を記入し、`Confirm merge` ボタンをクリックしましょう。

この際、`close` や `fix`, `resolve` などの単語に続けて課題番号を指定すると、当該課題が自動的にクローズされて便利です。詳細は下記リンクを参照してください。
- [How to Close Reference issues & pull request · gitbucket/gitbucket Wiki](https://github.com/gitbucket/gitbucket/wiki/How-to-Close-Reference-issues-&-pull-request)

![](./images/03_026.JPG)  

マージが完了するとプルリクエストされたブランチを削除するか確認されるので、削除して問題ない場合は `Delete branch` ボタンをクリックします。
![](./images/03_027.JPG)  

ブランチ削除の確認ダイアログが表示されるので `OK` ボタンをクリックします。
![](./images/03_028.JPG)  

当該ブランチが削除されたことを確認します。  
![](./images/03_029.JPG)

当該課題がクローズされたことを確認します。  
![](./images/03_030.JPG)

以上でプルリクエストのマージは完了です。


## マージ結果反映

最後に、サーバ上のリポジトリの状態と、開発端末にクローンしたリポジトリの状態を同期します。


### プル

サーバ上で `master` ブランチにマージされた内容を、開発端末にクローンした `masster` ブランチに反映します。`git checkout` コマンドで `master` ブランチに移動してください。
```sh
$ git checkout master
Switched to branch 'master'
```

`master` ブランチに移動したら `git pull` コマンドで `origin` リポジトリの内容を `master` ブランチにマージします。
```sh
$ git pull origin master
remote: Counting objects: 1, done
remote: Finding sources: 100% (1/1)
remote: Total 1 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (1/1), done.
From http://localhost:9999/git/root/sample
 * branch            master     -> FETCH_HEAD
   11ef662..107aaed  master     -> origin/master
Updating 11ef662..107aaed
Fast-forward
 src/main/java/sample/app/IndexController.java | 19 +++++++++++++++++++
 src/main/resources/templates/index.html       | 12 ++++++++++++
 2 files changed, 31 insertions(+)
 create mode 100644 src/main/java/sample/app/IndexController.java
 create mode 100644 src/main/resources/templates/index.html
```

`git log` コマンドに `--oneline` および `--graph` オプションを渡すと、コミットとマージの内容を簡潔に視覚的に確認することができます。 
```sh
$ git log --oneline --graph
*   107aaed Merge pull request #2 from root/issue/1_create_top_page
|\
| * ede0036 不要なコメントを削除 #1
| * 7c6c87e トップ画面を作成 #1
|/
* 11ef662 プロジェクトを作成
* 743b01d Initial commit
```

以上で `master` ブランチのプルは完了です。


### ブランチの削除

最後に、不要になったブランチを削除します。先ほど GitBucket 上で不要になったブランチを削除しましたが、この状態をローカルに反映していないため、`git branch -a` を実行するとローカルとリモートの双方に当該ブランチが表示されます。
```sh
$ git branch -a
  issue/1_create_top_page
* master
  remotes/origin/issue/1_create_top_page
  remotes/origin/master
```

まず、`git fetch` コマンドに削除されたブランチを反映する `--prune` を指定して実行します。
```sh
$ git fetch --prune
From http://localhost:9999/git/root/sample
 - [deleted]         (none)     -> origin/issue/1_create_top_page
```

リモートリポジトリに当該ブランチが表示されなくなったことを確認します。
```sh
$ git branch -a
  issue/1_create_top_page
* master
  remotes/origin/master
```

`git branch` コマンドに `-d` オプションと削除対象ブランチ名を指定して実行します。
```sh
$ git branch -d issue/1_create_top_page
Deleted branch issue/1_create_top_page (was ede0036).
```

ローカルリポジトリに当該ブランチが表示されなくなったことを確認します。
```sh
$ git branch -a
* master
  remotes/origin/master
```

以上でマージ結果の反映は完了です。

